``model`` module
================

.. automodule:: mllib.model.surrogate
    :members:
    :undoc-members:

.. automodule:: mllib.model.preprocessor
    :members:
    :undoc-members:

.. automodule:: mllib.model.kerassurrogate

.. automodule:: mllib.model.sklearnsurrogate

.. automodule:: mllib.model.spamssurrogate

.. automodule:: mllib.model.minmaxscaler

.. automodule:: mllib.model.preprocessorpipeline

.. automodule:: mllib.model.logarithmtransform

.. automodule:: mllib.model.centerandscale
