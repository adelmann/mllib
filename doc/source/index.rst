Welcome to mllib's documentation!
=================================

Below you can find a complete reference of all classes:

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   data
   model

Introductory example: Build a Keras surrogate model
---------------------------------------------------

.. code-block:: python
   
   import numpy as np
   from tensorflow import keras

   from mllib.model import KerasSurrogate, DummyPreprocessor
   
   # This code is taken from the Keras sequential model guide, see:
   # https://keras.io/getting-started/sequential-model-guide/.
   model = keras.models.Sequential()
   # Dense(64) is a fully-connected layer with 64 hidden units.
   # In the first layer, you must specify the expected input data shape:
   # Here, 20-dimensional vectors.
   model.add(keras.layers.Dense(64, activation='relu', input_dim=20))
   model.add(keras.layers.Dropout(0.5))
   model.add(keras.layers.Dense(64, activation='relu'))
   model.add(keras.layers.Dropout(0.5))
   model.add(keras.layers.Dense(10, activation='softmax'))

   sgd = keras.optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
   model.compile(loss='categorical_crossentropy',
                 optimizer=sgd,
                 metrics=['accuracy']);
 
   # generate some dummy data
   x_train = np.random.random((1000, 20))
   y_train = keras.utils.to_categorical(np.random.randint(10, size=(1000, 1)), num_classes=10)
   x_test = np.random.random((100, 20))
   y_test = keras.utils.to_categorical(np.random.randint(10, size=(100, 1)), num_classes=10)
   
   # define preprocessors
   preprocessor_x = DummyPreprocessor()
   preprocessor_y = DummyPreprocessor()

   # define the metadata
   version = 'Keras version: {}'.format(keras.__version__)
   name = 'example surrogate'
   
   # Build the surrogate model.
   surr = KerasSurrogate(model, preprocessor_x, preprocessor_y, name, version)
   
   # train the model (and the preprocessors!)
   training_args = {
     'epochs': 20,
     'batch_size': 128
   }
   
   surr.fit(x_train, y_train, **training_args)
   
   # save the model
   surr.save()
   
   # Load the model again.
   # The name must be the same as the name of the saved model!
   model = KerasSurrogate.load('models', name)

   # print some details
   print(model.details)

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
