import setuptools

setuptools.setup(
    name='mllib',
    version='0.1',
    author='Renato Bellotti',
    description='ML framework for internal use in AMAS at PSI',
    url='https://gitlab.psi.ch/adelmann/mllib/tree/master',
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
    install_requires=[
        'joblib',
        'tensorflow',
        'numpy',
        'pandas',
        'h5py',
        'keras',
        'sklearn',
        'pytz',
        'requests',
        'matplotlib', # needed in opalsourceinterpolator_test.py
        'tables',
    ]
)
