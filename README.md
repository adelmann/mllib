Documentation
=============

[![Documentation Status](https://readthedocs.org/projects/mllib/badge/?version=latest)](https://mllib.readthedocs.io/en/latest/?badge=latest)

Click the badge above to get to the Sphinx documentation of this package.

Running the examples
====================

Take a look at the ```examples/``` directory. You can also launch it on Binder:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.psi.ch%2Fadelmann%2Fmllib/master)

Installation
============

In the project root, execute the following command:

```
pip install .
```
For developers:
```
pip install --editable .
```

General Structure
=================
There are 2 packages:

- ```data```: Contains code responsible for data loading and querying
- ```model```: Contains the surrogate models

Examples
========

Usage examples can be found in the ```examples/``` subdirectory.