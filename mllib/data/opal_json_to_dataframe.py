import json
import pandas as pd


def design_variables_from_json(json_path):
    '''
    Converts the desgin variable samples in the given JSON file to a
    Pandas DataFrame.
    '''

    with open(json_path, 'r') as file:
        text = file.read()

        content = json.loads(text)
        samples = content['samples']

        keys = sorted(samples.keys())

        columns = samples['0']['dvar'].keys()

        df = pd.DataFrame(columns=columns)

        for key in keys:
            index = int(key)
            df.loc[index, :] = samples[key]['dvar']

        return df[df.columns].astype(float)
