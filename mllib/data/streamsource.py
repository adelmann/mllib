from abc import ABC, abstractmethod
from mllib.data import DataSource

class StreamSourceListener(ABC):
    '''Abstract baseclass used to get notified by a StreamClass when new data is available.'''
    @abstractmethod
    def on_new_data_arrived(self, event):
        '''
        Called whenever new data arrives.

        The type of ```event``` is determined by the implementing base class.
        '''
        raise NotImplementedError


class StreamSource(DataSource):
    '''
    Abstract base class for all real-time (i. e. stream) sources.

    This class notifies registered StreamListeners whenever new data arrives.
    '''
    def __init__(self):
        self._listeners = []

    @abstractmethod
    def connect_accelerator_stream(self, stream):
        '''Connects to the accelerator stream and starts notifying listeners.'''
        raise NotImplementedError

    @abstractmethod
    def disconnect_accelerator_stream(self):
        '''Disconnects from the accelerator stream and stops notifying listeners.'''
        raise NotImplementedError

    def add_stream_listener(self, l):
        '''
        :raises ValueError: if ```l``` is not of type :class:`StreamSourceListener`
        '''
        if isinstance(l, StreamSourceListener):
            self._listeners.append(l)
        else:
            raise ValueError

    def remove_stream_listener(self, l):
        '''
        Removes the listener.

        :raises ValueError: if the listener is not registered
        '''
        self._listeners.remove(l)

    def _notify_listeners(self, event):
        for l in self._listeners:
            l.on_new_data_arrived(event)
