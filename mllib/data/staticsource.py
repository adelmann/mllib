from mllib.data import DataSource


class StaticSource(DataSource):
    '''
    Base class for all static (i. e. non-realtime/non-stream) data sources.

    This class is just a flag interface, it does not provide any functionality.
    '''
