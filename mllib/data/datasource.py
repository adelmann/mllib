from abc import ABC, abstractmethod


class DataSource(ABC):
    '''
    Abstract base class for all data sources.

    The goal is to prove a unified interface to access data
    from different sources and various formats.
    '''
    @abstractmethod
    def select_dataset(self, identifier):
        '''
        Select a dataset provided by this DataSource.

        This function makes only sense for sources that provide multiple
        '''
        raise NotImplementedError

    @abstractmethod
    def set_view(self, constraints):
        '''Applies constraints for data selection.'''
        raise NotImplementedError

    @abstractmethod
    def get_data(self):
        '''Return the currently selected view on the dataset'''
        raise NotImplementedError

    @abstractmethod
    def iterate(self, chunksize):
        '''Returns a generator to iterate over the dataset in chunks.'''
        raise NotImplementedError

    @abstractmethod
    def get_available_design_variables(self):
        '''List available design variables / "X" values / features'''
        raise NotImplementedError

    @abstractmethod
    def get_available_qois(self):
        '''List available quantities of interest / QoI / "y" values'''
        raise NotImplementedError

    @property
    @abstractmethod
    def name(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def version(self):
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def list_datasets(cls):
        raise NotImplementedError
