import pandas as pd
import h5py
from mllib.data import StaticSource


class PandasHdfSource(StaticSource):
    '''Class representing a dataset stored as Pandas DataFrames in a single HDF5 file.'''
    def __init__(self, name, path):
        '''
        :param name: human readable name of this data source
        :param path: path to the HDF file to be loaded
        '''
        self._name = name
        self._path = path
        self._dvar = pd.read_hdf(self._path, 'dvar', mode='r')
        self._qoi = pd.read_hdf(self._path, 'qoi', mode='r')

    def select_dataset(self, identifier):
        '''Makes no sense for this type of source (contains only a single dataset).'''
        raise NotImplementedError

    def set_view(self, constraints):
        '''Not implemented because DataFrames allow easy manipulations directly.'''
        raise NotImplementedError

    def get_data(self):
        return self._dvar, self._qoi

    def iterate(self, chunksize):
        '''Not implemented yet'''
        # TODO
        raise NotImplementedError

    def get_available_design_variables(self):
        return self._dvar.columns

    def get_available_qois(self):
        return self._qoi.columns

    @property
    def name(self):
        return self._name

    @property
    def version(self):
        with h5py.File(self._path, 'r') as file:
            version = file['dvar'].attrs.get('pandas_version')
            return version.decode('ascii')

    @classmethod
    def list_datasets(cls):
        '''Makes no sense for this type of source (contains only a single dataset).'''
        raise NotImplementedError
