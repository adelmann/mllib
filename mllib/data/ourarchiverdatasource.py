import pandas as pd
import h5py
from datetime import datetime
import time
import os
from mllib.data import StaticSource


class OurArchiverDataSource(StaticSource):
    '''Class representing a dataset stored as Pandas DataFrames in a HDF5 files.'''
    def __init__(self, name, path):
        '''
        :param name: human readable name of this data source
        :param path: path to the directory where the HDF files to be loaded are stored
        '''
        self._name = name
        self._path = path

    def select_dataset(self, identifier):
        '''Makes no sense for this type of source (contains only a single dataset).'''
        raise NotImplementedError

    def set_view(self, constraints):
        '''Select the dataframes you want to load by filename. 
            :param constraints: list of the filenames of the .h5 files containing the dataframes to be loaded
        '''
        
        #self._time_constraints = constraints['time']
        #self._channel_constraints = constraints['channels']
        
        self._constraints = constraints
        

    def get_data(self):
        '''iterate through the  files in the directory choose the required ones and append them to the dataframe'''
        
        dataframes = []
        
        for file in os.listdir(self._path):
            if file in self._constraints:
                dataframes.append(pd.read_hdf(self._path+file))
                
        
        self._df = pd.concat(dataframes)
            
        return self._df

    def iterate(self, chunksize):
        '''Not implemented yet'''
        # TODO
        raise NotImplementedError

    def get_available_design_variables(self):
        raise NotImplementedError

    def get_available_qois(self):
        '''Returns the names of the loaded channels'''
        return self._df.columns

    @property
    def name(self):
        return self._name

    @property
    def version(self):
        with h5py.File(self._path, 'r') as file:
            version = file.attrs.get('pandas_version')
            return version.decode('ascii')

    @classmethod
    def list_datasets(cls):
        return self._constraints


