import numpy as np
import pandas as pd


class _Parameter:
    '''Represents a parameter in an output file. [Internal usage only!]'''

    def __init__(self, stringDescription):
        parts = stringDescription.strip().split(',')
        for part in parts:
            constituents = part.strip().split('=')
            if len(constituents) != 2:
                print('Unexpected Parameter format!')
                raise RuntimeError()

            prop = constituents[0]
            value = constituents[1]

            if prop == 'name':
                self._name = value
            elif prop == 'type':
                self._type = value
            elif prop == 'description':
                self._description = value

    def getName(self):
        return self._name

    def getType(self):
        return self._type

    def getDescription(self):
        return self._description


class _Column:
    '''Represents a column in a .stat file. [Internal usage only!]'''
    def __init__(self, stringDescription):
        parts = stringDescription.strip().split(',')
        for part in parts:
            constituents = part.split('=')
            if len(constituents) != 2:
                print('Unexpected Parameter format!')
                raise RuntimeError()

            prop = constituents[0]
            value = constituents[1].strip().strip('"')

            if prop == 'name':
                self._name = value
            elif prop == 'type':
                self._type = value
            elif prop == 'units':
                self._units = value
            elif prop == 'description':
                self._description = ' '.join(value.split(' ')[1:])
            else:
                print('Undefined column property!')
                raise RuntimeError()

    def getName(self):
        return self._name

    def getType(self):
        return self._type

    def getUnits(self):
        return self._units

    def getDescription(self):
        return self._description


class StatFile:
    '''Represents the contents of an OPAL .stat file. [Public API]'''

    def __init__(self, statFileName):
        self._parameters = []
        self._columns = []
        self._description = ''
        self._dataframe = None

        self._parse(statFileName)

    def _parse(self, fileName):
        with open(fileName) as file:
            line = file.readline()

            DESCRIPTION = 0
            PARAMETER = 1
            COLUMN = 2
            DATA = 3

            insideEnvironment = False
            environmentType = None
            environmentContent = ''

            numberOfLinesBeforeTable = 0

            while len(line) > 0:
                line = line.strip()

                numberOfLinesBeforeTable += 1
                if line == '&end':
                    # close the current environment
                    if not insideEnvironment:
                        print('Leaving environment without opening it!')
                        raise RuntimeError()
                    else:

                        if environmentType == DESCRIPTION:
                            self._description = environmentContent
                        elif environmentType == PARAMETER:
                            param = _Parameter(environmentContent)
                            self._parameters.append(param)
                        elif environmentType == COLUMN:
                            col = _Column(environmentContent)
                            self._columns.append(col)
                        elif environmentType == DATA:
                            pass
                        else:
                            print('Undefined environment state while ending!')
                            raise RuntimeError()

                        insideEnvironment = False
                        environmentType = None
                        environmentContent = ''

                elif line.startswith('&'):
                    if insideEnvironment:
                        print('Detected nested environments in {}!'.format(fileName))
                        raise RuntimeError()
                    else:
                        # start environment
                        insideEnvironment = True
                        environmentContent = ''

                        envType = line[1:].strip()

                        if envType == 'description':
                            environmentType = DESCRIPTION
                        elif envType == 'parameter':
                            environmentType = PARAMETER
                        elif envType == 'column':
                            environmentType = COLUMN
                        elif envType == 'data':
                            environmentType = DATA
                            pass
                        else:
                            print('Unknown environment type: {}'.format(envType))
                            raise RuntimeError()

                elif insideEnvironment:
                    environmentContent += line

                else:
                    if line == 'opal-t' or line == 'opal-cycl':
                        break

                # read the next line from the file
                line = file.readline()

            table = np.loadtxt(fileName, skiprows=numberOfLinesBeforeTable)

            assert table.shape[1] == len(self._columns)

            self._dataframe = pd.DataFrame()

            for i in range(len(self._columns)):
                self._dataframe.insert(
                    i, self._columns[i].getDescription(), table[:, i])

    def getDataFrame(self):
        '''Returns the QOI as a dataframe.'''
        return self._dataframe
