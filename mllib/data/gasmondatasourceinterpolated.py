import os
import pandas as pd
from mllib.data import StaticSource
import re


# +
class GasmonDataSourceInterpolated(StaticSource):

    def __init__(self, directory,excelFn, name):
        self._directory = directory
        self._excelFn = excelFn
        self._name = name
        
       

    def select_dataset(self, identifier):
        '''Not needed here'''
        raise NotImplementedError

    def set_view(self, constraints):
        '''
        Applies constraints for data selection.

        :param constraints: A dictionary of the following format:

            .. code-block:: python

                {
                    # select only the given quantities of interest
                    'qois': list,
                    #rename qois
                    'names': list    
                }

        '''
        for key, value in constraints.items():
            if key == 'names':
                self._names = value.copy()
            elif key == 'qois':
                self._selected_qois = value.copy()
            else:
                raise RuntimeError('WARNING: unknown constraint "{}"'.format(key))
                

    def get_data(self,doInterpolate=True, dropBadPulses=True, verbose=False,
                           CALCTthreshold=-50, CALCSthreshold=-50):
        
        first = True
        data = []
        for filename in sorted(os.listdir(self._directory)):
            if filename.endswith(".csv"):
                fntmp = re.sub(r'.*dp', '', filename)
                expNumber = re.sub(r'-nomeans.csv', '', fntmp)
                file_excel = pd.read_excel(self._excelFn)
                multVoltag = file_excel.iloc[int(expNumber)]['XeMultVoltag'] 
                try:
                    dp  = pd.read_csv(self._directory+filename, sep=";") 
                except:
                    print ("Can not read " + self._directory + filename)
                    continue
                dp = dp[self._selected_qois]
                dp.columns = self._names

                if doInterpolate:
                    dp['PHOTON-ENERGY-PER-PULSE'].interpolate(method='linear', 
                                                              inplace=True, 
                                                              limit_direction='forward', 
                                                              axis=0)
            
                dp = dp.dropna();
        
                # condition for bad pulse
                if dropBadPulses:
                    validT = dp['CALCT'] < CALCTthreshold
                    validS = dp['CALCS'] < CALCSthreshold
                    dp = dp[validT & validS]
            
                dp['XeMultVoltag'] = multVoltag
                dp['rawDataFile']  = filename
            
                if first:
                    data = dp
                    first = False
                else:
                    data = data.append(dp,ignore_index=True)

                if verbose:
                    print("Datapoint", expNumber, "gave", len(dp), "values")
                    
        data.reset_index(inplace=True, drop=True)
        data.dropna()
        return data    
        

    def iterate(self):
        '''Not implemented yet'''
        raise NotImplementedError 

    def get_available_design_variables(self):
        '''Not implemented yet'''
        raise NotImplementedError 

    def get_available_qois(self):
        '''Not implemented yet'''
        raise NotImplementedError 

    @property
    def name(self):
        return self._name

    @property
    def version(self):
        if not self._data_loaded:
            self._load_data()

        return self._version

    @classmethod
    def list_datasets(cls):
        '''Not implemented yet'''
        raise NotImplementedError

   
