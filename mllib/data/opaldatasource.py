import os
import json
import pandas as pd
from mllib.data import StaticSource
from .opal_json_to_dataframe import design_variables_from_json
from .opal_stat_file_to_dataframe import StatFile


class OpalDataSource(StaticSource):
    '''Class representing the output of an OPAL sampler run.'''

    opal_output_directories_path = None
    '''
    path to a directory where all the OPAL output files are stored
    (if they are stored in the some place)

    This path is used by :func:`list_datasets()`

    Default: None
    '''

    def __init__(self, opal_out_path, name, json_file=None, stat_file=None, use_cache=True):
        '''
        :param opal_out_path: directory where OPAL stores the output files
        :param name: name of this source
        :param stat_file: filename of the .stat files;
                            only needed if there is more than one .stat file
                            per OPAL run
        :param use_cache: if `True`, generates an HDF5 file that contains the
                            pandas DataFrames of the DVARs and the QOIs
        '''
        if opal_out_path.endswith('/'):
            # remove '/' at end
            opal_out_path = opal_out_path[:-1]

        self._opal_out_path = opal_out_path
        self._name = name
        self._last_s_only = True
        self._s_values = None

        # the JSON filename that contains the values of the design variables
        # only needed if multiple JSON files are present
        self._user_provided_json_file = json_file
        
        # .stat file name (needed if multiple .stat files will be present,
        # e. g. if YAG screens are used
        self._user_provided_stat_file = stat_file

        self._selected_qois = []

        self._data_loaded = False

        # to be read from the .stat file later
        self._version = None
        self._dvars = None
        self._qois = None

        # wheather to use an HDF5 file to store the extracted QOIs
        self._use_cache = use_cache

    def select_dataset(self, identifier):
        '''Not reasonable: An OPAL output directory defines one dataset'''
        raise NotImplementedError

    def set_view(self, constraints):
        '''
        Applies constraints for data selection.

        :param constraints: A dictionary of the following format:

            .. code-block:: python

                {
                    # select only the last step along s
                    'last_s_only': True/False (default: True),
                    # select only the given quantities of interest
                    # default: select all quantities of interest
                    'qois': list,
                }

        '''
        for key, value in constraints.items():
            if key == 'last_s_only':
                self._last_s_only = value
            elif key == 'qois':
                self._selected_qois = value.copy()
            else:
                raise RuntimeError('WARNING: unknown constraint "{}"'.format(key))

    def get_data(self):
        '''
        :returns:
            if last_s_only: DVAR (DataFrame), QOI (DataFrame)
                representing the last time step
            else: list(DVAR DataFrame), list(QOI DataFrame)
                representing all time steps
        '''
        if not self._data_loaded:
            self._load_data()

        return self._dvars, self._qois

    def iterate(self, chunksize):
        '''Only dummy implementation; still calls get_data() internally'''
        print('WARNING: OpalDataSource.iterate() is not tested yet!')
        dvar, qoi = self.get_data()
        num_rows = dvar.shape[0]

        start_index = 0

        while start_index <= num_rows:
            end_index = min(start_index + chunksize, num_rows)
            yield dvar.loc[start_index:end_index], qoi.loc[start_index:end_index]
            start_index = start_index + chunksize

    def get_available_design_variables(self):
        json_file = self._get_path_to_json()
        dvars = design_variables_from_json(json_file)
        cols = list(dvars.columns)
        cols.append('Path length')
        return cols

    def get_available_qois(self):
        stat_file_name = self._get_stat_file_name('{}/0'.format(self._opal_out_path))
        stat_file_path = '{}/0/{}'.format(self._opal_out_path, stat_file_name)

        file = StatFile(stat_file_path)
        data = file.getDataFrame()

        cols = list(data.columns)
        cols.remove('Path length')

        return cols

    @property
    def name(self):
        if not self._data_loaded:
            self._load_data()

        return self._name

    @property
    def version(self):
        if not self._data_loaded:
            self._load_data()

        return self._version

    @classmethod
    def list_datasets(cls):
        if cls.opal_output_directories_path:
            return os.listdir(cls.opal_output_directories_path)
        else:
            return None

    def _load_data(self):
        # load the qois and the design variables
        hdf_path = self.get_cache_path()

        json_file = self._get_path_to_json()

        # get version of OPAL
        with open(json_file, 'r') as file:
            text = file.read()
            data = json.loads(text)
            self._version = 'OPAL version: ' + data['OPAL version']
            self._version += '\n'
            self._version += 'Git revision: ' + data['git revision']

        if self._is_extracted():
            # load the data
            self._dvars, self._qois = self._load_from_hdf(hdf_path)
        else:
            # extract the data
            self._dvars, self._qois = self._extract_data(json_file)

            # save the data
            if self._use_cache:
                self._save_extracted()

        self._data_loaded = True

    def _extract_data(self, json_file):
        dvars = design_variables_from_json(json_file)
        dvars = dvars.sort_index()

        print('Successfully extracted design variables.')

        # number of samples
        N = dvars.shape[0]

        # each .stat file has the same name,
        # so get it by looking in the first directory
        stat_file_name = self._get_stat_file_name('{}/0'.format(self._opal_out_path))

        # list containing all dvar rows including duplicates for different s steps
        # of the same dvar configuration
        all_dvars = []
        all_qois = []

        for i in range(N):
            # print progress message
            if i % 100 == 0:
                print('Loading .stat file {} / {}'.format(i, N))

            # each directory containing a .stat file is named by a number
            stat_file_path = '{}/{}/{}'.format(self._opal_out_path, i, stat_file_name)

            # load the .stat file
            file = StatFile(stat_file_path)
            data = file.getDataFrame()

            # restrict the loaded QOIs to the columns of interest
            if self._selected_qois:
                columns = self._selected_qois
                # make sure that path length is in the QOIs so we can
                # shift it to the design variables in the end
                if 'Path length' not in columns:
                    columns.append('Path length')
                data = data[columns]

            # check if we need each s step or just the last one
            if self._last_s_only:
                data = data.tail(1)
            else:
                # create duplicates of the dvar rows to properly represent the
                # samples where multiple s steps share a common dvar configuration
                #
                # see:
                # http://www.datasciencemadesimple.com/repeat-or-replicate-the-dataframe-in-pandas-python/
                row = dvars.loc[i]
                all_dvars.append(row)

            all_qois.append(data)

        if self._last_s_only:
            # combine the design variables and QOIs for all the steps
            # into one DataFrame
            all_qois = pd.concat(all_qois)

            all_dvars = dvars

            # make sure the index is ascending integers for both DataFrames
            # so that simultaneous indexing (e. g. for filtering)
            # will not become a problem
            all_dvars.reset_index(drop=True, inplace=True)
            all_qois.reset_index(drop=True, inplace=True)

            # add the s column to the design variables,
            # then remove it from the qoi
            all_dvars['Path length'] = all_qois['Path length']
            all_qois.drop(columns='Path length', inplace=True)

        return all_dvars, all_qois

    def _get_path_to_json(self):
        # get the path to the JSON file
        json_file_candidates = [file for file in os.listdir(self._opal_out_path) if file.endswith('.json')]

        if len(json_file_candidates) == 0:
            raise RuntimeError('No JSON file in the OPAL output directory!')
        elif len(json_file_candidates) == 1:
            json_file = json_file_candidates[0]
        else:
            # multiple JSON files are present
            if not self._user_provided_json_file:
                raise RuntimeError(
                    'Multiple JSON files in OPAL output directory, please select one!'
                )
            else:
                json_file = self._user_provided_json_file

        return '{}/{}'.format(self._opal_out_path, json_file)

    def _get_stat_file_name(self, sample_dir):
        if self._user_provided_stat_file is not None:
            return self._user_provided_stat_file

        files_in_subdir = os.listdir(sample_dir)
        for file in files_in_subdir:
            if file.endswith('.stat'):
                return file
        raise RuntimeError('No .stat file found!')

    def get_cache_path(self):
        if self._last_s_only:
            file_name = 'extracted_data_last_s_only'
        else:
            file_name = 'extracted_data_all_s'

        if self._selected_qois:
            file_name += '_'.join(self._selected_qois)
        else:
            file_name += '_all_qois'

        file_name += '.hdf5'

        return '{}/{}'.format(self._opal_out_path, file_name)

    def _is_extracted(self):
        path = self.get_cache_path()

        files = os.listdir(self._opal_out_path)
        paths = ['{}/{}'.format(self._opal_out_path, file) for file in files]
        return (path in paths)

    def _save_extracted(self):
        hdf_path = self.get_cache_path()

        if self._last_s_only:
            self._dvars.to_hdf(hdf_path, key='dvar')
            self._qois.to_hdf(hdf_path, key='qoi')
        else:
            for i in range(len(self._dvars)):
                self._dvars[i].to_hdf(hdf_path, key='dvar_{}'.format(i))
                self._qois[i].to_hdf(hdf_path, key='qoi_{}'.format(i))

    def _load_from_hdf(self, hdf_path):
        if self._last_s_only:
            dvars = pd.read_hdf(hdf_path, key='dvar', mode='r')
            qois = pd.read_hdf(hdf_path, key='qoi', mode='r')
        else:
            # determine number of DataFrames to read
            with pd.HDFStore(hdf_path) as hdf:
                keys = hdf.keys()
                # keep only the dvar keys, i. e. those starting with '/dvar'
                dvar_keys = [k for k in keys if k.startswith('/dvar')]
                num_dvars = len(dvar_keys)

            # read the DataFrames and build a list of them
            dvars = []
            qois = []

            for i in range(num_dvars):
                dvar = pd.read_hdf(hdf_path, key='dvar_{}'.format(i))
                qoi = pd.read_hdf(hdf_path, key='qoi_{}'.format(i))

                dvars.append(dvar)
                qois.append(qoi)

        return dvars, qois
