import copy
import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
from mllib.data import StaticSource


class OpalSourceInterpolator(StaticSource):
    '''
    DataSource that allows to interpolate the output of an OPAL sampler run.
    '''

    def __init__(self, source):
        '''
        :param source: instance of ```OpalDataSource``` to interpolate
        '''
        source = copy.copy(source)
        source.set_view({'last_s_only': False})
        self._source = source

        # to be set in ```set_view```
        self._s = None
        self._kind = None
        self._steps_to_skip = None

    def select_dataset(self, identifier):
        '''Does not make sense for this class.'''
        raise NotImplementedError

    def set_view(self, s_values, steps_to_skip=0, kind='previous'):
        '''
        :param s_values: where to evaluate the source longitudinally
        :param steps_to_skip: number of steps that should be omitted for the
            interpolation (e. g. gun etc.) (default: 0)
        :param kind: kind of the fitting polynomial;
            must be odd (default: 'previous');
            for details see
            https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.interp1d.html#scipy.interpolate.interp1d
        '''
        if isinstance(s_values, float) or isinstance(s_values, int):
            s_values = [s_values]
        self._s = s_values
        self._kind = kind
        self._steps_to_skip = steps_to_skip

    def get_data(self):
        if self._s is None:
            raise RuntimeError('OpalSourceInterpolator.set_view() not called!')

        dvars, qois = self._source.get_data()

        interpolated_dvars = []
        interpolated_qois = []

        for i in range(len(dvars)):
            # get interpolation nodes
            s_fix = qois[i]['Path length'].values
            y_fix = qois[i].drop(columns='Path length').values
            # skip some values
            s_fix = s_fix[self._steps_to_skip:]
            y_fix = y_fix[self._steps_to_skip:, :]

            # interpolate
            f = interp1d(
                s_fix, y_fix,
                axis=0,
                kind=self._kind,
                bounds_error=False,
                fill_value='extrapolate')
            interpolated = f(self._s)

            # replicate the design variables to get into the following shape:
            # (n_samples, n_steps, n_dvars)
            design_variables_replicated = [dvars[i]]*interpolated.shape[0]
            design_variables_replicated = np.vstack(design_variables_replicated)
            
            # insert sample ID i as last feature column
            design_variables_replicated = np.hstack([design_variables_replicated, np.ones((design_variables_replicated.shape[0], 1)) * i])
            interpolated = np.hstack([interpolated, np.ones((interpolated.shape[0], 1)) * i])

            # add all samples from OPAL run nr. i to the list of all samples
            interpolated_dvars.append(design_variables_replicated)
            interpolated_qois.append(interpolated)

        # convert to shape (n_samples, n_s, n_features)
        interpolated_dvars = np.stack(interpolated_dvars, axis=0)
        interpolated_qois = np.stack(interpolated_qois, axis=0)

        return interpolated_dvars, interpolated_qois

    def iterate(self, chunksize):
        raise NotImplementedError

    def get_available_design_variables(self):
        return self._source.get_available_design_variables()

    def get_available_qois(self):
        return self._source.get_available_qois()

    @property
    def name(self):
        return self._source.name

    @property
    def version(self):
        return self._source.version

    @classmethod
    def list_datasets(cls):
        '''Does not make sense for this class.'''
        raise NotImplementedError


def convert_steps_to_samples(X, feature_column_labels, feature_to_append=None):
    '''
    Converts a numpy array of shape ```(n_samples, n_steps, n_features)```
    to a pandas DataFrame of shape ```(n_samples * n_steps, n_features)```

    :param X: numpy array of shape ```(n_samples, n_steps, n_features)```
    :param feature_column_labels: list of strings; length: ```n_features```
    :param feature_to_append: array of shape ```(n_steps)``;
        adds an additional feature to the samples as the LAST column,
        e. g. the values of s at each step
    '''
    n_samples = X.shape[0]
    n_steps = X.shape[1]
    n_features = X.shape[2]

    # Consistency check
    if feature_to_append is None:
        correct_num_labels = n_features
    else:
        correct_num_labels = n_features + 1

    if len(feature_column_labels) != correct_num_labels:
        raise RuntimeError(
            'Number of column labels must match number of features')

    samples = []
    for i in range(n_samples):
        # Convert all the steps for a single input sample to a feature matrix.
        # These matrices will be stacked below after adding an additional
        # column, if necessary.
        steps_for_single_input_sample = []
        for j in range(n_steps):
            steps_for_single_input_sample.append(X[i, j, :])
        # shape: (n_steps, n_features)
        steps_for_single_input_sample = np.vstack(steps_for_single_input_sample)
        # insert the additional feature column (if necessary)
        if feature_to_append is not None:
            steps_for_single_input_sample = np.insert(
                steps_for_single_input_sample,
                n_features,
                feature_to_append,
                axis=1)
        # append these samples to the list
        samples.append(steps_for_single_input_sample)

    # build the entire feature matrix by stacking, and build the DataFrame
    samples = np.vstack(samples)
    return pd.DataFrame(data=samples, columns=feature_column_labels)
