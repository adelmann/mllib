import joblib
import numpy as np
import pandas as pd
from mllib.model import Preprocessor
import random

class SlidingWindowSelection(Preprocessor):
    def __init__(self, window_length=5, distance_to_interlock_start=300, distance_to_interlock_end=25, distance_to_stable=3002, stride=1):
        '''
        !Attention: Rows containing any NaNs will be removed form the dataframe in transform()!

        :param window_length: number of timesteps to be included in one window. Default is 5, corresponding to 1s.
        :param distance_to_interlock_start: number of timesteps between the start of the window and the next interlock. Default is 300,
                                        corresponding to 1m.
        :param distance_to_interlock_end: number of timesteps between the end of the window and the next interlock. Default is 25,
                                        corresponding to 5s.
        :param distance_to_stable: number of timesteps between an interlock and the stable windows. Default is 3002,
                                        corresponding to 10m.
        :param stride: number of timesteps between adjacent windows. Default is 1, corresponding to 0.2s.
        '''

        self._wl = window_length
        self._dintlocks = distance_to_interlock_start
        self._dintlocke = distance_to_interlock_end
        self._dstable = distance_to_stable
        self._stride = stride

    def fit(self, X):
        #remove the NaNs
        X.dropna(axis=0,how='any',inplace=True)#drop all the rows containing any NaNs

        #get the channel keys
        self._channels = []
        for key in X.keys():
            if ':' in key:
                self._channels.append(key)

        #note the interlock indices
        interlocks = np.where(X['All'] == 1)[0]

        self._indices_intlock = [] #collect only the interlock indices to change the target labels later
        self._indices = []
        for j,i in enumerate(interlocks):
            #select the interlock window
            if i - self._dintlocks >= 0:
                start = i - self._dintlocks
                end = i - self._dintlocke
                ind_i = X.iloc[start:end].index.to_list()                    # index of interlock period i
                N_sw  = int((end - start - self._wl) / self._stride + 1) # Number of sliding windows for this interlock period
                for h in range(N_sw):
                    vert_start = h * self._stride
                    vert_end   = h * self._stride + self._wl
                    ind_i_h = ind_i[vert_start:vert_end]       # index of window "h" of interlock period "i"
                    self._indices.append(ind_i_h)
                    self._indices_intlock.append(ind_i_h)

            #select the stable window/s
            start = i+self._dstable
            if (i != interlocks[-1]):
                end = interlocks[j+1] - self._dstable
                ind_i = X.iloc[start:end].index.to_list()                    # index of stable period i
                N_sw  = int((end - start - self._wl) / self._stride + 1) # Number of sliding windows for this interlock period
                for h in range(N_sw):
                    vert_start = h * self._stride
                    vert_end   = h * self._stride + self._wl
                    ind_i_h = ind_i[vert_start:vert_end]       # index of window "h" of stable period "i"
                    self._indices.append(ind_i_h)

        #flatten the lists
        self._indices = [item for sublist in self._indices for item in sublist]
        self._indices_intlock = [item for sublist in self._indices_intlock for item in sublist]

    def transform(self, X):

        X = X.loc[self._indices,self._channels]
        X['TargetLabel'] = [0]*X.shape[0]
        X.loc[self._indices_intlock,'TargetLabel'] = 1

        #generate window indices
        index = []
        for i in range(int(X.shape[0]/self._wl)):
            for j in range(self._wl):
                index.append(i)

        assert(X.shape[0] == len(index))

        X['windowIndex'] = index

        return X



    def inverse_transform(self, X):
        raise NotImplementedError

    def save(self, filename):
        joblib.dump(self, filename)

    def get_version(self):
        return self._version

    @classmethod
    def load(cls, filename):
        return joblib.load(filename)

    @classmethod
    def get_fully_qualified_classname(cls):
        return 'mllib.model.WindowSelection'
