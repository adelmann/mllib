import os
import mllib    # needed for eval()
from mllib.model import Preprocessor


class PreprocessorPipeline(Preprocessor):
    '''Concatenates multiple preprocessors.'''

    def __init__(self, preprocessors=[]):
        self._preprocessors = preprocessors

    def add_preprocessor(self, p):
        self._preprocessors.append(p)

    def remove_preprocessor(self, p):
        self._preprocessors.remove(p)

    def fit(self, X):
        for preprocessor in self._preprocessors:
            preprocessor.fit(X)
            # transform X so that the next preprocessor can be fitted against
            # the transformed data
            X = preprocessor.transform(X)

    def transform(self, X):
        for preprocessor in self._preprocessors:
            X = preprocessor.transform(X)
        return X

    def inverse_transform(self, X):
        # be careful to apply the inverse transformations in inverse order!
        for preprocessor in reversed(self._preprocessors):
            X = preprocessor.inverse_transform(X)
        return X

    def save(self, filename):
        '''filename will contain the following information, where
        the information for each preprocessor is stored in a separate row:

        <fully qualified class name> ,  <file where the preprocessor is stored>

        The paths to a preprocessor save file are relative to the directory
        in which filename is saved.
        '''
        absolute_path = os.path.abspath(filename)
        file_name = os.path.basename(absolute_path)

        with open(filename, 'w') as file:
            for i, preprocessor in enumerate(self._preprocessors):
                # collect information needed to reconstruct this preprocessor
                save_file = '{}_{}.preprocessor'.format(file_name, i)
                class_name = preprocessor.get_fully_qualified_classname()

                # save the information needed to reconstruct this preprocessor
                file.write('{},{}\n'.format(class_name, save_file))

                # save the preprocessor
                preprocessor.save(f'{os.path.dirname(filename)}/{save_file}')

    def get_version(self):
        version = ''
        for i, preprocessor in enumerate(self._preprocessors):
            v = 'Preprocessor {}:\n'.format(i)
            v += preprocessor.get_version()
            version += v

    @classmethod
    def load(cls, filename):
        '''
        Basically the opposite method to ```PreprocessorPipelin.save()```.

        For details, see ```PreprocessorPipelin.save()```.
        '''
        preprocessors = []

        with open(filename, 'r') as file:
            for line in file.readlines():
                # parse the line
                class_name, save_file = line.split(',')
                class_name = class_name.strip()
                save_file = save_file.strip()

                # build an absolute path to the save file
                save_file = f'{os.path.dirname(filename)}/{save_file}'

                # load the instance of the preprocessor
                class_instance = eval(class_name)
                preprocessor = class_instance.load(save_file)
                preprocessors.append(preprocessor)

        return PreprocessorPipeline(preprocessors)

    @classmethod
    def get_fully_qualified_classname(cls):
        return 'mllib.model.PreprocessorPipeline'

    def __eq__(self, other):
        if not isinstance(other, PreprocessorPipeline):
            return NotImplemented

        for i, preprocessor in enumerate(self._preprocessors):
            if preprocessor != other._preprocessors[i]:
                return False

        return True
