import joblib
import numpy as np
import pandas as pd
from mllib.model import Preprocessor
import random

class WindowSelection(Preprocessor):
    def __init__(self,window_length=1476, distance_to_interlock=25, distance_to_stable=3002):
        '''
        !Attention: Rows containing any NaNs will be removed form the dataframe in transform()!
        
        :param window_length: number of timesteps to be included in one window. Default is 1476, corresponding to 4m55s.
        :param distance_to_interlock: number of timesteps between the end of the window and the interlock. Default is 25,
                                        corresponding to 5s.
        :param distance_to_stable: number of timesteps between an interlock and the stable windows. Default is 3002,
                                        corresponding to 10m.
        '''
       
        self._wl = window_length
        self._dintlock = distance_to_interlock
        self._dstable = distance_to_stable
        

    def fit(self, X):
        #remove the NaNs
        X.dropna(axis=0,how='any',inplace=True)#drop all the rows containing any NaNs
        
        #get the channel keys
        self._channels = []
        for key in X.keys():
            if ':' in key:
                self._channels.append(key) 
                
        #note the interlock indices
        interlocks = np.where(X['All'] == 1)[0]
        
          
        
        self._indices_intlock = [] #collect only the interlock indices to change the target labels later
        self._indices = []
        for j,i in enumerate(interlocks):
            #select the interlock window
            start = i -(self._wl+self._dintlock)
            end = i - self._dintlock
            
            self._indices.append(X.iloc[start:end].index.to_list())
            self._indices_intlock.append(X.iloc[start:end].index.to_list()) 
            
            #select the stable window/s
            start = i+self._dstable 
            end = start + self._wl
            #self._indices.append(X.iloc[start:end].index.to_list())
            if(i != interlocks[-1]):

                while(end+(self._wl+1)) < (interlocks[j+1]-self._dstable):
                    
                    self._indices.append(X.iloc[start:end].index.to_list())
                    start = end+1 
                    
                    end = start + self._wl
                    
                    #self._indices.append(X.iloc[start:end].index.to_list())     
        #flatten the lists
        self._indices = [item for sublist in self._indices for item in sublist]
        self._indices_intlock = [item for sublist in self._indices_intlock for item in sublist]
    
    def transform(self, X):
        
        X = X.loc[self._indices,self._channels]
        X['TargetLabel'] = [0]*X.shape[0]
        X.loc[self._indices_intlock,'TargetLabel'] = 1
    
        #generate window indices
        index = []
        for i in range(int(X.shape[0]/self._wl)):
            for j in range(self._wl):
                index.append(i)
                
        assert(X.shape[0] == len(index))
        
        X['windowIndex'] = index
        
        return X
       
            

    def inverse_transform(self, X):
        raise NotImplementedError 

    def save(self, filename):
        joblib.dump(self, filename)

    def get_version(self):
        return self._version

    @classmethod
    def load(cls, filename):
        return joblib.load(filename)

    @classmethod
    def get_fully_qualified_classname(cls):
        return 'mllib.model.WindowSelection'


