import sklearn
import joblib
from sklearn.preprocessing import QuantileTransformer
from mllib.model import Preprocessor

class QuantileTransformerPreprocessor(Preprocessor):
    '''MLLIB Preprocessor wrapper for the sklearn class QuantileTransformer'''

    def __init__(self, **kwargs):
        ''':param kwargs: parameters to pass to the constructor of QuantileTransformer'''
        self._scaler = QuantileTransformer(**kwargs)
        self._version = 'scikit-learn version: {}'.format(sklearn.__version__)

    def fit(self, X):
        self._scaler.fit(X)

    def transform(self, X):
        return self._scaler.transform(X)

    def inverse_transform(self, X):
        return self._scaler.inverse_transform(X)

    def save(self, filename):
        joblib.dump(self, filename)

    def get_version(self):
        return self._version

    @classmethod
    def load(cls, filename):
        return joblib.load(filename)

    @classmethod
    def get_fully_qualified_classname(self):
        return 'mllib.model.QuantileTransformerPreprocessor'

    def __eq__(self, other):
        if not isinstance(other, QuantileTransformerPreprocessor):
            return NotImplemented

        print(self._version)
        print(other._version)

        return (self._scaler == other._scaler) and (self._version == other._version)
