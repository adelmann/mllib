import numpy as np
import joblib

from mllib.model import Surrogate

class SpamsSurrogate(Surrogate):
    '''
    Surrogate representing a spams model

    expects model as a dictionary of the form

    .. code-block:: python

       model = {
        'model_name':<name of the method>,
        'model_params': <dictionary with model parameters>
       }
    E. g.:

    .. code-block:: python

       model = {
        'model_name': spams.fistaFlat,
        'model_params': param
       }
     '''

    def _fit_model(self, X_train, y_train, X_val=None, y_val=None, **kwargs):
        #initialize the weights
        W0 = np.asfortranarray(np.ones((X.shape[1], 1)), dtype=float)

        X = np.asfortranarray(X)
        y = np.asfortranarray(y)

        #fit the model
        (self.weights, self.opt_info) = self.model['model_name'](y, X, W0, **self.model['model_params'])

    @property
    def details(self):
        raise NotImplementedError

    def _predict_model(self, X):
        self.weights = self.weights[:, 0]

        return np.dot(self.weights, X.T)

    def _save_model(self, model_dir):
        model_file = '{}/model.pickle'.format(model_dir)
        joblib.dump(self.model, model_file)

    @classmethod
    def _load_model(cls, model_dir):
        model_file = '{}/model.pickle'.format(model_dir)
        model = joblib.load(model_file)
        return model

    @classmethod
    def _build_surrogate(cls, model, preprocessor_x, preprocessor_y, name, version, model_dir):
        return SpamsSurrogate(model, preprocessor_x, preprocessor_y, name, version)
