import joblib
import numpy as np
from mllib.model import Preprocessor


class LogarithmTransform(Preprocessor):
    '''Takes the (natural) logarithm of the input data.'''

    def __init__(self, columns=None):
        '''
        :param columns: List of column indices to apply the transform to
                        If `None`: transform all columns
                        default: `None`
        '''
        self._columns = columns

    def fit(self, X):
        # select all columns if not explicitly specified by the user
        if self._columns is None:
            self._columns = np.arange(X.shape[1])

    def transform(self, X):
        to_return = X.copy()
        to_return[:, self._columns] = np.log(X[:, self._columns])
        return to_return

    def inverse_transform(self, X):
        to_return = X.copy()
        to_return[:, self._columns] = np.exp(X[:, self._columns])
        return to_return

    def save(self, filename):
        joblib.dump(self, filename)

    def get_version(self):
        '''Nothing to do: a stateless object'''
        return '1.0'

    @classmethod
    def load(cls, filename):
        return joblib.load(filename)

    @classmethod
    def get_fully_qualified_classname(cls):
        return 'mllib.model.LogarithmTransform'

    def __eq__(self, other):
        if not isinstance(other, LogarithmTransform):
            return NotImplemented

        return self._columns == other._columns
