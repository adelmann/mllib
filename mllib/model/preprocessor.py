from abc import ABC, abstractmethod


class Preprocessor(ABC):
    @abstractmethod
    def fit(self, X):
        raise NotImplementedError

    @abstractmethod
    def transform(self, X):
        '''
        :raises RuntimeError: if there was no prior fitting
        '''
        raise NotImplementedError

    @abstractmethod
    def inverse_transform(self, X):
        '''
        :raises RuntimeError: if there was no prior fitting
        '''
        raise NotImplementedError

    @abstractmethod
    def save(self, filename):
        raise NotImplementedError

    @abstractmethod
    def get_version(self):
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def load(cls, filename):
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def get_fully_qualified_classname(cls):
        raise NotImplementedError


class DummyPreprocessor(Preprocessor):
    '''
    Identity transformation; does nothing.

    Use this when code expects a preprocessor, but you don't need one.
    '''

    def fit(self, X):
        pass

    def transform(self, X):
        return X

    def inverse_transform(self, X):
        return X

    def save(self, filename):
        pass

    def get_version(self):
        return 'Identity transform has no version.'

    @classmethod
    def load(cls, filename):
        return DummyPreprocessor()

    @classmethod
    def get_fully_qualified_classname(cls):
        return 'mllib.model.DummyPreprocessor'

    def __eq__(self, other):
        '''
        this function is needed because we want to use DummyPreprocessor
        in tests, where (in)equality needs to be tested for, and returns False
        if it is not implemented
        '''
        if not isinstance(other, DummyPreprocessor):
            return NotImplemented

        return True
