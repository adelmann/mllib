import joblib
import numpy as np
from mllib.model import Preprocessor

class ShiftPositive(Preprocessor):
    '''
    Preprocessor that shifts all the values to the positive real halfaxis.
    
    Does so by shifting all columns by their minimum value to the right.
    '''

    def __init__(self, offset=1e-14):
        '''
        :param offset: How much bigger the values should be than zero;
                        offset=1 means that all values are > 1.
                        Default: 1e-14
        '''
        self._shift = None
        self._offset = offset

    def fit(self, X):
        if len(X.shape) == 1:
            self._shift = np.zeros((1, X.shape[0]))
            self._shift = self._shift = np.abs(np.min(X, axis=0))
        elif len(X.shape) == 2:
            self._shift = np.abs(np.min(X, axis=0))
            self._shift = np.reshape(self._shift, (1, X.shape[1]))
        else:
            raise RuntimeError('Only 1D or 2D arrays are allowed.')

        # add the offset
        self._shift += self._offset

    def transform(self, X):
        return X + self._shift

    def inverse_transform(self, X):
        return X - self._shift

    def save(self, filename):
        joblib.dump(self, filename)

    def get_version(self):
        # No external ML library is needed.
        return '1.0.0'

    @classmethod
    def load(cls, filename):
        return joblib.load(filename)

    @classmethod
    def get_fully_qualified_classname(self):
        return 'mllib.model.ShiftPositive'
