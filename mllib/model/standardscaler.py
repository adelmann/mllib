import sklearn
import joblib
import pandas as pd
from mllib.model import Preprocessor

class StandardScaler(Preprocessor):
    '''
    Preprocessor providing standardisation.

    The data will be transformed in a way so that it has mean 0
    and standard deviation 1.
    '''

    def __init__(self):
        self._scaler = sklearn.preprocessing.StandardScaler()
        self._version = 'scikit-learn version: {}'.format(sklearn.__version__)

    def fit(self, X):
        self._scaler.fit(X)

    def transform(self, X):
        transformed =  self._scaler.transform(X)
        if isinstance(X, pd.DataFrame):
            return pd.DataFrame(data=transformed, columns=X.columns)
        else:
            return transformed

    def inverse_transform(self, X):
        transformed = self._scaler.inverse_transform(X)
        if isinstance(X, pd.DataFrame):
            return pd.DataFrame(data=transformed, columns=X.columns)
        else:
            return transformed

    def save(self, filename):
        joblib.dump(self, filename)

    def get_version(self):
        return self._version

    @classmethod
    def load(cls, filename):
        return joblib.load(filename)

    @classmethod
    def get_fully_qualified_classname(cls):
        return 'mllib.model.StandardScaler'
