import joblib
import numpy as np
import pandas as pd
from mllib.model import Preprocessor
import random

class TrainTestValSplit(Preprocessor):
    def __init__(self,num_splits, remove_attributes =True, retain_timeorder_over_sets = True,
                 one_target_per_window = False):
        '''
        Splits the dataframe in Train, Test and Validation sets.  
    
            :params num_splits: number of splits performed on the data. At the moment only 2,3 are accepted values.
            :params retain_timeorder_over_sets: if True time order is preserved accross the splits
            :params remove_attributes: if set to True, a dataframe containing only the channel data 
                                        will be returned. Columns corresponding to channels are identified by 
                                        the presence of ':' in the column name.
            :params one_target_per_window: if True one target will be returned per window instead of a list of
                                            length equal to the window length.
        '''
        self._numsplits = num_splits
        self._modelready = remove_attributes
        self._retain_timeorder = retain_timeorder_over_sets
        self._oney = one_target_per_window
        
        assert (self._numsplits == 2 or self._numsplits == 3), 'Invalid number of splits'
        

    def fit(self, X):
        ''' Sets the indices for each split '''       
        #sample the wanted proportions for each set
        random.seed(42)
        if(self._retain_timeorder == False):
            
            self.train = []
            self.test = []
            self.val = []

            index = X['windowIndex'].unique()
            
            
            if self._numsplits == 2:
                for j in index:
                    proba = np.random.rand()
                    if(proba <=0.7):
                        self.train.append(j)
                    else:
                        self.test.append(j)
            
            elif self._numsplits == 3:
                for j in index:
                    proba = np.random.rand()
                    if(proba <=0.7):
                        self.train.append(j)
                    elif(0.7<proba<=0.85):
                        self.test.append(j)
                    else:
                        self.val.append(j)
             
                        
        elif(self._retain_timeorder == True):
            if self._numsplits == 2:
                train = int(X.shape[0]*0.7)
                
                train = X['windowIndex'].iloc[train] #last window index of the train set
                self.train = [i for i in range(train)]
                
                test = X['windowIndex'].iloc[-1]
                self.test = [i for i in range(train,test+1)]
                self.test = list(set(self.test).difference(self.train))
                
                        
            elif self._numsplits == 3:
                train = int(X.shape[0]*0.7)
                test = int(X.shape[0]*0.15)
    
                train_index = X['windowIndex'].iloc[train] #last window index of the train set
                self.train = [i for i in range(train_index)]
            
                test_index = X['windowIndex'].iloc[train+test] #last window index of the train set
                self.test = [i for i in range(train_index,test_index+1)]
                self.test = list(set(self.test).difference(self.train))
                
                val = X['windowIndex'].iloc[-1]
                self.val = [i for i in range(test_index,val+1)]
                self.val = list(set(self.val).difference(self.test))
                
                
            
    
    def transform(self, X):
        '''Cut X into the splits defined in fit()'''
        if(self._numsplits == 3):
            X_train = X.loc[(X['windowIndex'].isin(self.train))]
            X_test = X.loc[(X['windowIndex'].isin(self.test))]
            X_val = X.loc[(X['windowIndex'].isin(self.val))]
            
            if(self._oney == True):
                y_train = flatten_y(X_train)
                y_test = flatten_y(X_test)
                y_val = flatten_y(X_val)
            
            if not(self._oney == True):
                y_train = X_train['TargetLabel'].astype('int32')
                y_test = X_test['TargetLabel'].astype('int32')
                y_val = X_val['TargetLabel'].astype('int32')
        
            if(self._modelready == True):
                #select the channel data
                channels = []
                for key in X.keys():
                    if ':' in key:
                        channels.append(key)
                       
                X_train = X_train.loc[:,channels]
                X_test = X_test.loc[:,channels]
                X_val = X_val.loc[:,channels]
        
            return X_train,y_train, X_test,y_test, X_val, y_val
        
        elif(self._numsplits ==2):
            X_train = X.loc[(X['windowIndex'].isin(self.train))]
            X_test = X.loc[(X['windowIndex'].isin(self.test))]

            if not(self._oney == True):
                y_train = X_train['TargetLabel'].astype('int32')
                y_test = X_test['TargetLabel'].astype('int32')
            
            if(self._oney == True):
                y_train = flatten_y(X_train)
                y_test = flatten_y(X_test)
            
            if(self._modelready == True):
                #select the channel data
                channels = []
                for key in X.keys():
                    if ':' in key:
                        channels.append(key)
                       
                X_train = X_train.loc[:,channels]
                X_test = X_test.loc[:,channels]
                
            return X_train,y_train, X_test,y_test
            

    def inverse_transform(self, X):
        raise NotImplementedError 

    def save(self, filename):
        joblib.dump(self, filename)

    def get_version(self):
        return self._version

    @classmethod
    def load(cls, filename):
        return joblib.load(filename)

    @classmethod
    def get_fully_qualified_classname(cls):
        return 'mllib.model.TrainTestSplit'


def flatten_y(df):
    y = []
    for i in range(df['windowIndex'].unique()[0], df['windowIndex'].unique()[-1]+1):
        y.append(df.loc[df['windowIndex']==i,['TargetLabel']].iloc[0].values[0])
    return y
