import os
from abc import ABC, abstractmethod
# the following 2 lines are needed for "eval"
import mllib

class Surrogate(ABC):
    '''
    Abstract baseclass for all surrogate models.

    A surrogate model learns the mapping f(X) = y.
    '''
    def __init__(self, model, preprocessor_x, preprocessor_y, name, version):
        self._model = model
        self.preprocessor_x = preprocessor_x
        self.preprocessor_y = preprocessor_y
        self._name = name
        self._version = version

    def fit(self, X_train, y_train, X_val=None, y_val=None, **kwargs):
        '''
        Fits a model (and the preprocessors) to the given data: f(X) = y.

        :param kwargs: keyword arguments depend on the concrete implementation,
            they can be e. g. batch size etc.
        '''
        # fit the preprocessors
        self.preprocessor_x.fit(X_train)
        self.preprocessor_y.fit(y_train)

        # transform the data
        X_train = self.preprocessor_x.transform(X_train)
        y_train = self.preprocessor_y.transform(y_train)

        if X_val is not None:
            X_val = self.preprocessor_x.transform(X_val)
            y_val = self.preprocessor_y.transform(y_val)

        # fit the model
        self._fit_model(X_train, y_train, X_val, y_val, **kwargs)

    @abstractmethod
    def _fit_model(self, X_train, y_train, X_val=None, y_val=None, **kwargs):
        '''Fits the model only.'''
        raise NotImplementedError

    def predict(self, X, **kwargs):
        X = self.preprocessor_x.transform(X)
        prediction = self._predict_model(X, **kwargs)
        return self.preprocessor_y.inverse_transform(prediction)

    @abstractmethod
    def _predict_model(self, X, **kwargs):
        '''
        :param kwargs: keyword arguments depend on the concrete implementation,
            they can be e. g. batch size etc.

        :raise RuntimeError: if the model has not been fitted to training data yet
        '''
        raise NotImplementedError

    def save(self, directory='models'):
        '''
        Saves this model and the corresponding preprocessor.

        :param directory: The directory where the models are stored in.
            Each model creates a subdirectory named by ```self.name```
            where it is stored.
        '''
        # everything will be stored in this directory
        model_dir = '{}/{}'.format(directory, self._name)

        if not os.path.exists(model_dir):
            os.makedirs(model_dir)

        # save the underlying model
        self._save_model(model_dir)

        # save preprocessor
        preprocessor_path_x = '{}/preprocessor_x.preprocessor'.format(model_dir)
        self.preprocessor_x.save(preprocessor_path_x)

        preprocessor_path_y = '{}/preprocessor_y.preprocessor'.format(model_dir)
        self.preprocessor_y.save(preprocessor_path_y)

        # also save classname of the preprocessor so we know
        # which of the subclasses of preprocessor's load() method to call
        # in Surrogate.load()

        preprocessor_classname_x = self.preprocessor_x.get_fully_qualified_classname()
        preprocessor_classname_y = self.preprocessor_y.get_fully_qualified_classname()
        preprocessor_classname_path = '{}/preprocessor_classes.txt'.format(model_dir)

        with open(preprocessor_classname_path, 'w') as file:
            file.write(preprocessor_classname_x)
            file.write('\n')
            file.write(preprocessor_classname_y)

        # save the version
        version_path = '{}/version.txt'.format(model_dir)
        with open(version_path, 'w') as file:
            file.write(self._version)

    @property
    @abstractmethod
    def details(self):
        '''
        :returns: dict -- hyperparameter values, architecture etc.
        '''
        raise NotImplementedError

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, value):
        self._version = value

    @property
    def model(self):
        return self._model

    @model.setter
    def model(self, m):
        self._model = m

    @classmethod
    def load(cls, directory, name, model_kwargs={}):
        model_dir = '{}/{}'.format(directory, name)

        # load the model
        model = cls._load_model(model_dir, model_kwargs)

        # load the preprocessors
        preprocessor_classname_path = '{}/preprocessor_classes.txt'.format(model_dir)
        with open(preprocessor_classname_path, 'r') as file:
            preprocessor_class_x = eval(file.readline())
            preprocessor_class_y = eval(file.readline())

        preprocessor_path_x = '{}/preprocessor_x.preprocessor'.format(model_dir)
        preprocessor_path_y = '{}/preprocessor_y.preprocessor'.format(model_dir)

        preprocessor_x = preprocessor_class_x.load(preprocessor_path_x)
        preprocessor_y = preprocessor_class_y.load(preprocessor_path_y)

        # load the version
        version_path = '{}/version.txt'.format(model_dir)
        with open(version_path, 'r') as file:
            version = file.read()

        return cls._build_surrogate(model, preprocessor_x, preprocessor_y, name, version, model_dir)

    @abstractmethod
    def _save_model(self, model_dir):
        '''Save the underlying model.'''
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def _load_model(cls, model_dir):
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def _build_surrogate(cls, model, preprocessor_x, preprocessor_y, name, version, model_dir):
        raise NotImplementedError
