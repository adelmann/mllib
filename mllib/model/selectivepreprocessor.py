import os
import json
import mllib    # needed for eval()
from mllib.model import Preprocessor

class SelectivePreprocessor(Preprocessor):
    '''
    Filter that allows the apply the contained preprocessing only to some columns.
    '''

    def __init__(self, preprocessor, column_indices_to_transform):
        '''
        :param preprocessor: The transformation that should only be applied
                             to some columns, but not all
        :param column_indices_to_transform: the columns that should be transformed
        '''
        self._preprocessor = preprocessor
        self._column_indices_to_transform = column_indices_to_transform.copy()

    def fit(self, X):
        self._preprocessor.fit(X)

    def transform(self, X):
        X_copy = X.copy()

        transformed = self._preprocessor.transform(X)
        X_copy[:, self._column_indices_to_transform] = transformed[:, self._column_indices_to_transform]
        del transformed

        return X_copy

    def inverse_transform(self, X):
        X_copy = X.copy()

        transformed = self._preprocessor.inverse_transform(X)
        X_copy[:, self._column_indices_to_transform] = transformed[:, self._column_indices_to_transform]
        del transformed

        return X_copy

    def save(self, filename):
        absolute_path = os.path.abspath(filename)
        directory = os.path.dirname(absolute_path)
        file_name = os.path.basename(absolute_path)

        # save the information needed to reconstruct the inner preprocessor
        save_file = f'{directory}/{file_name}_inner.preprocessor'
        index_file = f'{directory}/{file_name}_indices.json'
        with open(filename, 'w') as file:
            class_name = self._preprocessor.get_fully_qualified_classname()
            file.write(f'{class_name},{save_file},{index_file}')

        with open(index_file, 'w') as file:
            json.dump(self._column_indices_to_transform, file)

        # save the preprocessor
        self._preprocessor.save(save_file)

    def get_version(self):
        return '1.0'

    @classmethod
    def load(cls, filename):
        with open(filename, 'r') as file:
            line = file.read()
            # parse the line
            class_name, save_file, index_file = line.split(',')
            class_name = class_name.strip()
            save_file = save_file.strip()
            index_file = index_file.strip()

        # load the instance of the preprocessor
        class_instance = eval(class_name)
        preprocessor = class_instance.load(save_file)

        # load the list of indices to transform
        with open(index_file, 'r') as file:
            indices_to_transform = json.load(file)

        return SelectivePreprocessor(preprocessor, indices_to_transform)

    @classmethod
    def get_fully_qualified_classname(cls):
        return 'mllib.model.SelectivePreprocessor'
