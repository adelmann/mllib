import joblib
import pandas as pd
import sklearn
from sklearn.preprocessing import MinMaxScaler
from mllib.model import Preprocessor


class AdaptiveMinMaxScaler(Preprocessor):
    def __init__(self, a=0, b=1):
        '''
        Scales data linearly to the range [a, b]:

        y = alpha * x + beta,

        where alpha, beta are determined by the data given to :function:`fit()`.
        '''
        self._scaler = MinMaxScaler(feature_range=(a, b))
        self._version = 'scikit-learn version: {}'.format(sklearn.__version__)

    def fit(self, X):
        self._scaler.fit(X)

    def transform(self, X):
        transformed = self._scaler.transform(X)
        if isinstance(X, pd.DataFrame):
            return pd.DataFrame(data=transformed, columns=X.columns)
        else:
            return transformed

    def inverse_transform(self, X):
        transformed = self._scaler.inverse_transform(X)
        if isinstance(X, pd.DataFrame):
            return pd.DataFrame(data=transformed, columns=X.columns)
        else:
            return transformed

    def save(self, filename):
        joblib.dump(self, filename)

    def get_version(self):
        return self._version

    @classmethod
    def load(cls, filename):
        return joblib.load(filename)

    @classmethod
    def get_fully_qualified_classname(cls):
        return 'mllib.model.AdaptiveMinMaxScaler'


class FixedMinMaxScaler(Preprocessor):
    def __init__(self, bounds, columns, a=0, b=1):
        '''
        Scales data linearly to the range [a, b]:

        y = alpha * x + beta,

        where alpha, beta are determined by the user in the constructor.

        :param bounds: boundaries of the data BEFORE the scaling is applied;
            ```dict``` of the form

        {
            'column 1': (min1, max1),
            'column 2': (min2, max2),
            ...
        }

        :param columns: labels of the columns in the data to be transformed
        :param a: desired lower bound for the variables AFTER the scaling
        :param b: desired upper bound for the variables AFTER the scaling
        '''
        self._a = a
        self._b = b
        self._target_width = b - a

        self._bounds = bounds
        self._columns = columns

        self._input_widths = {}
        for key, value in bounds.items():
            self._input_widths[key] = value[1] - value[0]

        self._version = 'Implementation version 1'

    def fit(self, X):
        pass

    def transform(self, X):
        '''
        :param X: a numpy array
        '''
        input_2D = self._is_input_2D(X)

        output = X.copy()
        for i, col in enumerate(self._columns):
            if col not in self._bounds.keys():
                print('Cannot scale columns "{}" - no bounds given!'.format(col))
                continue

            if input_2D:
                # shift input to zero
                output[:, i] = output[:, i] - self._bounds[col][0]
                # scale input to the desired range
                output[:, i] = output[:, i] * self._target_width / self._input_widths[col]
                # shift to the desired offset
                output[:, i] = output[:, i] + self._a
            else:
                # shift input to zero
                output[:, :, i] = output[:, :, i] - self._bounds[col][0]
                # scale input to the desired range
                output[:, :, i] = output[:, :, i] * self._target_width / self._input_widths[col]
                # shift to the desired offset
                output[:, :, i] = output[:, :, i] + self._a

        return output

    def inverse_transform(self, X):
        '''
        :param X: a numpy array
        '''
        input_2D = self._is_input_2D(X)

        # do the opposite of :function:`transform()`
        output = X.copy()
        for i, col in enumerate(self._columns):
            if col not in self._bounds.keys():
                print('Cannot scale columns "{}" - no bounds given!'.format(col))
                continue

            if input_2D:
                output[:, i] = output[:, i] - self._a
                output[:, i] = output[:, i] * self._input_widths[col] / self._target_width
                output[:, i] = output[:, i] + self._bounds[col][0]
            else:
                output[:, :, i] = output[:, :, i] - self._a
                output[:, :, i] = output[:, :, i] * self._input_widths[col] / self._target_width
                output[:, :, i] = output[:, :, i] + self._bounds[col][0]

        return output

    def save(self, filename):
        joblib.dump(self, filename)

    def get_version(self):
        return self._version

    @classmethod
    def load(cls, filename):
        return joblib.load(filename)

    @classmethod
    def get_fully_qualified_classname(cls):
        return 'mllib.model.FixedMinMaxScaler'

    def _is_input_2D(self, X):
        if len(X.shape) == 2:
            input_2D = True
        elif len(X.shape) == 3:
            # we have to deal with time series data of shape:
            # (n_samples, n_time_steps, n_features)
            input_2D = False
        else:
            raise RuntimeError('Input data must be 2D or 3D!')

        return input_2D
