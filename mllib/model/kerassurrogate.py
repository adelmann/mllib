import os       # needed for eval()
from tensorflow import keras
import mllib    # needed for eval()
from mllib.model import Surrogate


class KerasSurrogate(Surrogate):
    '''Base class for surrogate models using Keras.'''

    def _fit_model(self, X_train, y_train, X_val, y_val, **kwargs):
        '''Fit Keras model.'''
        # fit the model
        self.model.fit(X_train, y_train, validation_data=(X_val, y_val), **kwargs)

    def _predict_model(self, X, **kwargs):
        return self.model.predict(X, **kwargs)

    def _save_model(self, model_dir):
        # save neural network
        model_path = '{}/model.hdf5'.format(model_dir)
        self.model.save(model_path)

    @property
    def details(self):
        return {
            'name': self.name,
            'version': self.version,
            'summary': self.model.summary(),
            'learning_rate': self.model.optimizer.lr,
            'optimizer': self.model.optimizer.__class__.__name__,
        }

    def __str__(self):
        msg = """
            Keras Surrogate model
            Name: {}
            Version: {}
            preprocessor for X: {}
            preprocessor for y: {}
            """

        return msg.format(
            self.name,
            self.version,
            self.preprocessor_x.__class__,
            self.preprocessor_y.__class__
        )

    @classmethod
    def _load_model(cls, model_dir, model_kwargs={}):
        model_kwargs = model_kwargs.copy()
        model_kwargs['compile'] = model_kwargs.get('compile', True)

        model_path = '{}/model.hdf5'.format(model_dir)
        model = keras.models.load_model(model_path, **model_kwargs)

        return model

    @classmethod
    def _build_surrogate(cls, model, preprocessor_x, preprocessor_y, name, version, model_dir):
        return KerasSurrogate(model, preprocessor_x, preprocessor_y, name, version)
