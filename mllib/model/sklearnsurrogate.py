import joblib
from mllib.model import Surrogate


class SklearnSurrogate(Surrogate):
    '''Surrogate model for wrapping an sklearn model'''

    def _fit_model(self, X_train, y_train, X_val=None, y_val=None, **kwargs):
        # fit the model
        self.model.fit(X_train, y_train)

    def _predict_model(self, X, **kwargs):
        return self.model.predict(X)

    def predict_proba(self, X, **kwargs):
        X = self.preprocessor_x.transform(X)
        prediction = self.model.predict_proba(X)

        return self.preprocessor_y.inverse_transform(prediction)

    def _save_model(self, model_dir):
        model_file = '{}/model.pickle'.format(model_dir)
        joblib.dump(self.model, model_file)

    @classmethod
    def _load_model(cls, model_dir):
        model_file = '{}/model.pickle'.format(model_dir)
        model = joblib.load(model_file)
        return model

    @classmethod
    def _build_surrogate(cls, model, preprocessor_x, preprocessor_y, name, version, model_dir):
        return SklearnSurrogate(model, preprocessor_x, preprocessor_y, name, version)

    @property
    def details(self):
        return {}
