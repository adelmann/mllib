import joblib
import numpy as np
import pandas as pd
import sklearn
from sklearn.preprocessing import StandardScaler
from mllib.model import Preprocessor

class Standardize(Preprocessor):
    def __init__(self, remove_attributes = True):
        '''
        Centers and scales each feature x independently as:

        x = (x-mean(x))/std(x)

        :params remove_nans: if ```True``` ```NaN```s are removed inplace.
            (only applied for a pandas ```Dataframe```)
        '''
        self._scaler = StandardScaler()
        self._version = 'scikit-learn version: {}'.format(sklearn.__version__)
        self._remove_attributes = remove_attributes

        # set in fit()
        self._first_channel = None
        self._last_channel = None
        self._data_keys = None

    def fit(self, X):
        '''
        Fits X.

        If X is a pandas DataFrame it is expected to be in the format
        ```[*channel_data, 'globalDate', *attributes]```.
        Only the channel data will be fitted.
        '''
        if isinstance(X, pd.DataFrame):
            #select the channel data
            self._channels = []
            self._attributes = []
            for key in X.keys():
                if ':' in key:
                    self._channels.append(key)
                else:
                    self._attributes.append(key)    
                    
            self._scaler.fit(X.loc[:,self._channels])
            
        else:    
            self._scaler.fit(X)

    def transform(self, X):
        if isinstance(X, pd.DataFrame):
            transformed = self._scaler.transform(X.loc[:,self._channels])
            if(self._remove_attributes == False):
                transformed = np.hstack((transformed, X.loc[:,self._attributes].values))
                return pd.DataFrame(data=transformed, columns=X.columns)
            elif(self._remove_attributes == True):
                return pd.DataFrame(data=transformed, columns=self._channels)
        else:
            transformed = self._scaler.transform(X)
            return transformed

    def inverse_transform(self, X):
        if isinstance(X, pd.DataFrame):
            transformed = self._scaler.inverse_transform(X.loc[:,self._channels])
            transformed = np.hstack((transformed, X.loc[:,self._attributes].values))

            return pd.DataFrame(data=transformed, columns=X.columns)
        else:
            transformed = self._scaler.inverse_transform(X)
            return transformed

    def save(self, filename):
        joblib.dump(self, filename)

    def get_version(self):
        return self._version

    @classmethod
    def load(cls, filename):
        return joblib.load(filename)

    @classmethod
    def get_fully_qualified_classname(cls):
        return 'mllib.model.Standardize'
