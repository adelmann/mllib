import os
import unittest
import numpy as np
from mllib.model import LogarithmTransform, SelectivePreprocessor

class LogarithmTransformTest(unittest.TestCase):

    def setUp(self):
        # path to test saving and loading
        self.savefile = 'selective_preprocessor_transform_test.pk'
        # make sure the savd file is deleted before each test
        if os.path.exists(self.savefile):
            os.remove(self.savefile)

        # columns to transform
        self.to_transform1 = [1, 3]
        self.to_transform2 = [2]

        # generate the test data in a reproducible way
        self.test_data = np.array([[1., 1., 1., 1.],
                                   [2., 4., 8., 16.]])
        #np.random.seed(439840)
        #self.test_data = np.random.uniform(0.0001, 10592, (42, 6))
        self.correctly_transformed1 = self.test_data.copy()
        self.correctly_transformed1[:, 1] = np.log(self.test_data[:, 1])
        self.correctly_transformed1[:, 3] = np.log(self.test_data[:, 3])

        self.correctly_transformed2 = self.test_data.copy()
        self.correctly_transformed2[:, 2] = np.log(self.test_data[:, 2])

        # instantiate the transformation
        self.trafo1 = SelectivePreprocessor(LogarithmTransform(), self.to_transform1)
        self.trafo2 = SelectivePreprocessor(LogarithmTransform(), self.to_transform2)

        self.trafo1.fit(self.test_data)
        self.trafo2.fit(self.test_data)

    def test_fit(self):
        preprocessor = LogarithmTransform()
        preprocessor.fit = unittest.mock.MagicMock()
        preproc = SelectivePreprocessor(preprocessor, self.to_transform1)

        preproc.fit(self.test_data)

        preprocessor.fit.assert_called_with(self.test_data)

    def test_transform(self):
        '''test for ```LogarithmTransform.transform()```'''
        transformed1 = self.trafo1.transform(self.test_data)
        transformed2 = self.trafo2.transform(self.test_data)

        self.assertTrue(np.isclose(transformed1, self.correctly_transformed1).all())
        self.assertTrue(np.isclose(transformed2, self.correctly_transformed2).all())

    def test_consistency(self):
        '''test for ```LogarithmTransform.inverse_transform()```'''
        transformed = self.trafo1.transform(self.test_data)
        transformed = self.trafo1.inverse_transform(transformed)

        self.assertTrue(np.isclose(transformed, self.test_data).all())

    def test_saving(self):
        '''test for ```LogarithmTransform.save()``` and ```LogarithmTransform.load()```'''
        transformed_before_loading = self.trafo1.transform(self.test_data)

        self.trafo1.save(self.savefile)
        loaded = SelectivePreprocessor.load(self.savefile)

        transformed_after_loading = loaded.transform(self.test_data)

        self.assertTrue(np.isclose(transformed_before_loading, transformed_after_loading).all())

if __name__ == '__main__':
    unittest.main()
