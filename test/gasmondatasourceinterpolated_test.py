import os
import unittest
import numpy as np
import pandas as pd
from mllib.data import GasmonDataSourceInterpolated

# + {"active": ""}
# df = pd.read_csv('../../ml-gasmon/cleaned/dp01-nomeans.csv',sep=';')

# + {"active": ""}
# df[:15].to_csv('gasmon_test_data/dp01-nomeans.csv',sep=';')
# -

class TestGasmonDataSourceInterpolated(unittest.TestCase):
    def setUp(self):
        data_dir = 'gasmon_test_data/'
        excelFn = data_dir +'XeX77.xlsx'
        self.data_source = GasmonDataSourceInterpolated(data_dir,excelFn, name='test')
        
        # reference values for output
        correct_data = {
            'CALCT': [-241.18384672, -265.61645254, -267.84396544, -230.04707127,-246.6649698],
            'CALCS': [-760.29733095, -851.1065454 , -898.569758  , -718.69005468,-802.25705159],
            'SPECTRUM_CENTER': [6105.20871539, 6101.94233369, 6103.31412162, 6101.06880764,6104.8940575],
            'PHOTON-ENERGY-PER-PULSE': [258.54660702, 258.54660702, 258.54660702, 258.54660702,
       258.54660702],
            'XeMultVoltag': [926.57659089, 926.57659089, 926.57659089, 926.57659089,926.57659089],
            'rawDataFile': ['dp01-nomeans.csv', 'dp01-nomeans.csv', 'dp01-nomeans.csv',
       'dp01-nomeans.csv','dp01-nomeans.csv']
            
        }
    
        self.correct_data = pd.DataFrame(correct_data, columns = correct_data.keys())
        
    def test(self):
        constraints = {
            'qois': ['SARFE10-PBIG050-EVR0:CALCT.value', 
                    'SARFE10-PBIG050-EVR0:CALCS.value',
                    'SARFE10-PSSS059:SPECTRUM_CENTER.value',
                    'SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG.value'],
            'names':['CALCT','CALCS','SPECTRUM_CENTER','PHOTON-ENERGY-PER-PULSE']
        }
        self.data_source.set_view(constraints)
        data = self.data_source.get_data()
       
        self.assertTrue(self.correct_data.shape == data.shape)
        
        for i in data.keys().to_list()[:-1]:
            self.assertTrue(np.isclose(self.correct_data[i],data[i]).all())
    

if __name__ == '__main__':
    unittest.main()
