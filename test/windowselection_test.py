import unittest
import numpy as np
import pandas as pd
from mllib.model import WindowSelection

# +
class WindowSelectionTest(unittest.TestCase):
    def setUp(self):
        np.random.seed(329749530)
        normal_data = np.random.normal(loc=5., scale=33, size=45)
        normal_data2 = np.random.normal(loc=2, scale=100, size=45)
        a = [0, 0, 0, 0,0,0,0 ,0, 1, 0, 0, 0,0,0,0,0,0,0,0,0,0,0 ,1, 0, 0, 0, 0, 0, 
             0,0,0,0, 1, 0, 0, 0, 0,0,0,0,0 ,1, 0, 0, 0]
        
        data = {
            'normal:col1': normal_data,
            'normal:col2': normal_data2,
            'All' : a
        }
   
        
        self.df = pd.DataFrame(data)
        
        self.num_interlocks = 4
        self.num_stable = 7
        
        self.wl=1
        self.timetoint = 2
        self.distance_to_stable = 2
        
    
    def test_number_of_windows(self):
        windows = WindowSelection(window_length=self.wl,distance_to_interlock=self.timetoint,
                                 distance_to_stable=self.distance_to_stable)
        windows.fit(self.df)
        df = windows.transform(self.df)
                        

        self.assertTrue(self.num_interlocks == df.loc[df['TargetLabel'] == 1].shape[0])    
        self.assertTrue(self.num_stable == df.loc[df['TargetLabel'] == 0].shape[0]) 
        
    def test_window_size(self):
        windows = WindowSelection(window_length=self.wl,distance_to_interlock=self.timetoint,
                                 distance_to_stable=self.distance_to_stable)
        windows.fit(self.df)
        df = windows.transform(self.df)
        
        for cnt in df['windowIndex'].unique():
            self.assertTrue(self.wl == df.loc[df['windowIndex']==cnt].shape[0])
        

   
     
# -





if __name__ == '__main__':
    unittest.main()
