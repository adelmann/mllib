import os
import unittest
import numpy as np
import matplotlib.pyplot as plt
from mllib.data import OpalDataSource, OpalSourceInterpolator

class TestOpalSourceInterpolator(unittest.TestCase):

    def setUp(self):
        self.plot_dir = 'OpalSourceInterpolator_plots'
        self.data_dir = 'opal_test_data'
        
        if not os.path.exists(self.plot_dir):
            os.makedirs(self.plot_dir)

        self._opal = OpalDataSource(self.data_dir, 'test')
        self._source = OpalSourceInterpolator(self._opal)

    def test_shape(self):
        s = [0., 1., 2.]
        num_timesteps = len(s)
        # characteristics of the test data
        num_samples = 12
        # +1 because of sample ID that is added by the OpalSourceInterpolator
        num_dvar_features = 10 + 1
        num_qoi_features = 41 + 1 # not 42 because the Path length has been removed!

        self._source.set_view(s)
        dvar, qoi = self._source.get_data()

        self.assertTrue(dvar.shape == (num_samples, num_timesteps, num_dvar_features))
        self.assertTrue(qoi.shape == (num_samples, num_timesteps, num_qoi_features))

    def test_at_interpolation_points_of_each_sample(self):
        self._opal.set_view({'last_s_only': False})
        dvar_fix, qoi_fix = self._opal.get_data()

        differences = []

        for i in range(len(qoi_fix)):
            qoi_fix[i]['sample_id'] = i
            s = qoi_fix[i]['Path length']
            self._source.set_view(s)

            dvar_interp, qoi_interp = self._source.get_data()
            diff = np.linalg.norm(
                qoi_fix[i].drop(columns='Path length').values - qoi_interp[i]
            )
            differences.append(diff)

        maximum = max(differences)
        self.assertTrue(maximum <= 1e-8)

    def test_dvars(self):
        self._opal.set_view({'last_s_only': False})
        dvar_fix, qoi_fix = self._opal.get_data()

        s = np.linspace(0, 18.2, int(18.2 / 0.01))
        self._source.set_view(s)
        dvar, qoi = self._source.get_data()

        # check if the design variables have been replicated correctly
        # the shape of dvar and qoi is (n_samples, n_steps, n_features)
        for i in range(dvar.shape[0]):
            first_row = dvar[i, 0, :]
            for j in range(dvar.shape[1]):
                self.assertTrue(np.isclose(dvar[i, j, :], first_row).all())

    def test_visually(self):
        s = np.linspace(0, 18.4, int(18.4 / 0.01))

        self._opal.set_view({'last_s_only': False})
        dvar_true, qoi_true = self._opal.get_data()

        self._source.set_view(s, kind='previous')
        for i in range(len(qoi_true)):
            dvar = dvar_true[i]
            qoi = qoi_true[i]
            dvar_interp, qoi_interp = self._source.get_data()
            qoi_cols = self._source.get_available_qois()

            # energy
            # i-th sample, all time steps
            index = qoi_cols.index('Mean Bunch Energy')

            fig, ax = plt.subplots()
            ax.plot(qoi['Path length'], qoi['Mean Bunch Energy'], 'x', label='True')
            ax.plot(s, qoi_interp[i, :, index], label='interpolated')
            fig.legend()
            fig.savefig('{}/energy_{}.png'.format(self.plot_dir, i))
            plt.close(fig)

            # emittance_x
            index = qoi_cols.index('Normalized Emittance x')

            fig, ax = plt.subplots()
            ax.plot(qoi['Path length'], qoi['Normalized Emittance x'], 'x', label='True')
            ax.plot(s, qoi_interp[i, :, index], label='interpolated')
            fig.legend()
            fig.savefig('{}/emittance_x_{}.png'.format(self.plot_dir, i))
            plt.close(fig)

            # RMS_x
            index = qoi_cols.index('RMS Beamsize in x')

            fig, ax = plt.subplots()
            ax.plot(qoi['Path length'], qoi['RMS Beamsize in x'], 'x', label='True')
            ax.plot(s, qoi_interp[i, :, index], label='interpolated')
            fig.legend()
            fig.savefig('{}/RMS_beamsize_x_{}.png'.format(self.plot_dir, i))
            plt.close(fig)

if __name__ == '__main__':
    unittest.main()
