import os
import shutil
import unittest
import numpy as np
from mllib.model import QuantileTransformerPreprocessor

class QuantileTransformerPreprocessorTest(unittest.TestCase):
    def setUp(self):
        self.save_directory = 'test_quantile_transformer_save'
        self.save_file = self.save_directory + '/' + 'save_file.preprocessor'
        if os.path.exists(self.save_directory):
            shutil.rmtree(self.save_directory)

        os.makedirs(self.save_directory)

        np.random.seed(9438099)
        self.dummy_data = np.random.uniform(size=(2000, 100))

    def test_fit(self):
        '''
        Just tests if the `fit()` function raises an Error or Exception
        '''
        preprocessor = QuantileTransformerPreprocessor()
        preprocessor.fit(self.dummy_data)

    def test_transform(self):
        '''
        Just tests if the `transform()` function raises an Error or Exception
        '''
        preprocessor = QuantileTransformerPreprocessor()
        preprocessor.fit(self.dummy_data)
        preprocessor.transform(self.dummy_data)

        # TODO: add semantic tests as well

    def test_inverse_transform(self):
        '''
        Just tests if the `inverse_transform()` function raises an Error or Exception
        '''
        preprocessor = QuantileTransformerPreprocessor()
        preprocessor.fit(self.dummy_data)
        preprocessor.inverse_transform(self.dummy_data)

        # TODO: add semantic tests as well

    def test_save_load(self):
        # save
        preprocessor = QuantileTransformerPreprocessor()
        preprocessor.fit(self.dummy_data)
        transformed_before_saving = preprocessor.transform(self.dummy_data)
        preprocessor.save(self.save_file)
        # load
        loaded = QuantileTransformerPreprocessor.load(self.save_file)
        transformed_after_saving = loaded.transform(self.dummy_data)

        self.assertTrue((transformed_before_saving == transformed_after_saving).all())


if __name__ == '__main__':
    unittest.main()
