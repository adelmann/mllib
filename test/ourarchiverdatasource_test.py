import os
import unittest
import numpy as np
import pandas as pd
import h5py
from mllib.data import OurArchiverDataSource

class TestOurArchiverDataSource(unittest.TestCase):
    def setUp(self):
        path = 'ourarchiver_test_data/'


        self.data_source = OurArchiverDataSource(name='test_ourarchiverdatasource',path = path)
        
        self.constraints = ['sample_2019-09-18.h5','sample_2019-09-19.h5','sample_2019-09-20.h5']
            

        #reference values for quantities for interest
        self._true_qoi = ['AHA:IST:2','AHD1:IST:2','All','Others']
        
        index = np.array([1568764800000000000, 1568764800200000000, 1568764800400000000,
            1568764800600000000, 1568764800800000000,1568851200000000000, 1568851200200000000, 1568851200400000000,
            1568851200600000000, 1568851200800000000,1568937600000000000, 1568937600200000000, 1568937600400000000,
            1568937600600000000, 1568937600800000000],
           dtype='int64')
        
        self._true_values = { 'AHA:IST:2': np.array([3674.01660156, 3673.99609375, 3674.0012207 , 3673.98071289,
       3674.01123047,3672.6027832 , 3672.61279297, 3672.57226562, 3672.62792969,
       3672.60742188,3672.328125  , 3672.30761719, 3672.28710938, 3672.37402344,
       3672.39916992]),
                             
                              'AHD1:IST:2' : np.array([339.86114502, 339.8550415 , 339.84893799, 339.8550415 ,
       339.87335205,339.86114502, 339.84893799, 339.84283447, 339.84893799,
       339.8550415,339.87335205, 339.87335205, 339.86724854, 339.84893799,
       339.86114502]),
                             'All': np.array([0, 0, 0, 0, 0,0, 0, 0, 0, 0,0, 0, 0, 0, 0]),
                             'Others': np.array([0, 0, 0, 0, 0,0, 0, 0, 0, 0,0, 0, 0, 0, 0])
                            }
                             
                                                 
        
        self._true_df = pd.DataFrame(self._true_values, columns = self._true_qoi ,index=index)
       
    def test_set_view(self):
        
        self.data_source.set_view(self.constraints)
        self.assertTrue(self.constraints == self.data_source._constraints)

    def test_get_data(self):
        self.data_source.set_view(self.constraints)
        self.dataframe = self.data_source.get_data()
        
        self.assertTrue(self._true_df.shape == self.dataframe.shape)

if __name__ == '__main__':
    unittest.main()
