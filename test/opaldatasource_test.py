import os
import unittest
import numpy as np
import pandas as pd
from mllib.data import OpalDataSource

class TestOpalDataSource(unittest.TestCase):
    def setUp(self):
        data_dir = 'opal_test_data'

        # remove cache files from previous runs, if any
        for file in os.listdir(data_dir):
            if file.endswith('.hdf5'):
                os.remove('{}/{}'.format(data_dir, file))

        self.data_source = OpalDataSource(data_dir, 'test_dataset')

        # reference values for design variables
        correct_dvar_data = {
            'IBF': [
                469.94384053328508,
                419.6103879517288,
                465.54044283847031,
                490.94698269578788,
                413.28165215701796,
                456.38804182237345,
                498.70547192387306,
                433.8771745623294,
                479.65468115259881,
                408.1753005882216,
                448.57909118393911,
                430.85734000791092
            ],
            'IM': [
                418.38247056497983,
                384.90463155337051,
                263.54680220659293,
                302.25356990770604,
                437.0453294610279,
                287.14758055479092,
                353.91937242789186,
                320.09653750149334,
                336.70185211952844,
                404.05295911449883,
                370.39490188297822,
                274.18797593067228
            ],
            'GPHASE': [
                -14.817571348078262,
                -3.3024695341967814,
                -0.49377291094628362,
                -9.1500513651021187,
                -25.213953539498142,
                -22.556436192067324,
                -21.283975229336878,
                -17.925914372852358,
                -10.21708988356707,
                -7.1502705605637757,
                -28.787676167831879,
                -16.722757819045473
            ],
            'FWHM': [
                6.0927156855979719e-12,
                8.5851501702906917e-12,
                9.8759986044710949e-12,
                1.9441991943231928e-12,
                3.0342378704952146e-12,
                5.027516110838992e-12,
                4.0766500283729877e-12,
                7.8885717583486572e-12,
                6.6114310306171436e-12,
                2.6420887225040081e-12,
                7.7535838962957608e-12,
                5.0971579620818256e-12
            ],
            'KQ1': [
                -5.2693319342033433,
                6.555703391120538,
                2.0765255638218463,
                4.5591513854278567,
                3.8325003231288211,
                -1.394100036527063,
                7.9770941664794073,
                -0.10080349799017618,
                -3.1127131660348715,
                1.2224198428353095,
                -7.3119413171815815,
                -6.3706546744251522
            ],
            'KQ2': [
                6.1795764686298824,
                3.3603900255469874,
                -6.8592166562869128,
                -3.5996993761931755,
                7.7511856388234026,
                -4.8717826901228678,
                0.7511050465593101,
                -2.0971336840847723,
                -0.69879140046076316,
                4.9728807675367435,
                2.1385180533034269,
                -5.9631178163644414
            ],
            'KQ3': [
                -1.7939138640648977,
                4.988829528608834,
                -0.96233349821001823,
                -5.4849826544109952,
                7.2996091439886417,
                0.32308319713292022,
                -5.0613810987614345,
                3.9196478858705763,
                -7.5163637537882009,
                5.8890965253838576,
                1.374322182886111,
                -2.9110896150117611
            ],
            'KQ4': [
                -0.8973441492770533,
                -3.1168888481354884,
                3.2580008803391109,
                5.6089624096396324,
                5.1435680278075839,
                7.6973194937034179,
                -4.3345669866787402,
                -7.8717222363481598,
                1.4944764520662659,
                -1.644735015713584,
                0.71428512670978606,
                -6.3771105900819229
            ],
            'p1': [
                -20.42858863847043,
                -6.8376758262843467,
                -0.82132903783389821,
                -16.680050951393675,
                -13.214306453630634,
                -8.906809990595935,
                -28.66271399302067,
                -4.5191204710008961,
                -18.471209675862458,
                -10.339049670233976,
                -25.733559451827869,
                -23.926355516815146
            ],
            'PHASE': [
                0.70077507291145857,
                0.65799391863767553,
                0.50291138724084605,
                0.55237456196626855,
                0.72462424206914522,
                0.53307069767738546,
                0.61839801908153758,
                0.57517599634401362,
                0.59639584049800787,
                0.68246346564737015,
                0.63945201145887953,
                0.51650968713666956
            ]
        }

        self.correct_dvar_last_s = pd.DataFrame(correct_dvar_data)

        # the reference design variables for the case where multiple s are wanted
        # is basically the same as for the case where only the last s is wanted,
        # but as a list
        lst = []

        for row in range(self.correct_dvar_last_s.shape[0]):
            to_append = self.correct_dvar_last_s.loc[row]
            lst.append(to_append)

        self.correct_dvar_all_s = lst

        # reference values for quantities of interest
        correct_columns = [
                'Time',
                'Path length',
                'Number of Macro Particles',
                'Bunch Charge',
                'Mean Bunch Energy',
                'RMS Beamsize in x',
                'RMS Beamsize in y',
                'RMS Beamsize in s',
                'RMS Normalized Momenta in x',
                'RMS Normalized Momenta in y',
                'RMS Normalized Momenta in s',
                'Normalized Emittance x',
                'Normalized Emittance y',
                'Normalized Emittance s',
                'Mean Beam Position in x',
                'Mean Beam Position in y',
                'Mean Beam Position in s',
                'x coordinate of reference particle in lab cs',
                'y coordinate of reference particle in lab cs',
                'z coordinate of reference particle in lab cs',
                'x momentum of reference particle in lab cs',
                'y momentum of reference particle in lab cs',
                'z momentum of reference particle in lab cs',
                'Max Beamsize in x',
                'Max Beamsize in y',
                'Max Beamsize in s',
                'Correlation xpx',
                'Correlation ypy',
                'Correlation zpz',
                'Dispersion in x',
                'Derivative of dispersion in x',
                'Dispersion in y',
                'Derivative of dispersion in y',
                'Bx-Field component of ref particle',
                'By-Field component of ref particle',
                'Bz-Field component of ref particle',
                'Ex-Field component of ref particle',
                'Ey-Field component of ref particle',
                'Ez-Field component of ref particle',
                'energy spread of the beam',
                'time step size',
                'outside n*sigma of the beam',
        ]

        correct_data_all_rows = []
        correct_data_last_rows = []

        for i in range(12):
            stat_file_path = '{}/{}/awa.stat'.format(data_dir, i)
            all_rows = np.loadtxt(stat_file_path, skiprows=279)
            last_row = all_rows[-1,:]

            all_rows = pd.DataFrame(data=all_rows, columns=correct_columns)

            correct_data_all_rows.append(all_rows)
            correct_data_last_rows.append(last_row)

        self.correct_qoi_all_rows = correct_data_all_rows

        self.correct_data_last_rows = np.vstack(correct_data_last_rows)
        self.correct_qoi_last_rows = pd.DataFrame(data=correct_data_last_rows, columns=correct_columns)

        # shift 'Path length' from the qoi to the dvar
        self.correct_dvar_last_s['Path length'] = self.correct_qoi_last_rows['Path length']
        self.correct_qoi_last_rows.drop(columns='Path length', inplace=True)

    def test_dvar(self):
        dvar, qoi = self.data_source.get_data()
        self.assertTrue(len(self.correct_dvar_last_s) == len(dvar))
        
        self.assertTrue(self.correct_dvar_last_s.equals(dvar))

    def test_get_available_design_variables(self):
        true_cols = self.correct_dvar_last_s.columns
        obtained_cols = self.data_source.get_available_design_variables()

        self.assertTrue((obtained_cols == true_cols).all().all())

    def test_qoi_last_row(self):
        dvar, qoi = self.data_source.get_data()
        self.assertTrue(self.correct_qoi_last_rows.equals(qoi))

    def test_get_all_steps(self):
        self.data_source.set_view({'last_s_only': False})
        dvar, qoi = self.data_source.get_data()

        # make sure the dimensions are correct
        self.assertTrue(len(dvar) == len(qoi))
        self.assertTrue(len(dvar) == len(self.correct_dvar_all_s))

        for i, correct_dvar_row in enumerate(self.correct_dvar_all_s):
            self.assertTrue(correct_dvar_row.equals(dvar[i]))
            self.assertTrue(self.correct_qoi_all_rows[i].equals(qoi[i]))

    def test_get_available_qois(self):
        true_cols = self.correct_qoi_last_rows.columns
        obtained_cols = self.data_source.get_available_qois()
        self.assertTrue((obtained_cols == true_cols).all().all())

    def test_set_qoi_of_interest(self):
        test_cols = [
            'Number of Macro Particles',
            'Mean Bunch Energy',
            'RMS Beamsize in x',
            'RMS Beamsize in y',
            'RMS Beamsize in s',
            'Normalized Emittance x',
            'Normalized Emittance y',
            'Normalized Emittance s',
            'energy spread of the beam'
        ]
        
        self.data_source.set_view({'qois': test_cols})
        dvar, qoi = self.data_source.get_data()

        self.assertTrue(self.correct_dvar_last_s.equals(dvar))
        self.assertTrue(self.correct_qoi_last_rows[test_cols].equals(qoi))

if __name__ == '__main__':
    unittest.main()
