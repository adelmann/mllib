import os
import shutil
import unittest
import numpy as np
from mllib.model import DummyPreprocessor, LogarithmTransform, PreprocessorPipeline

class PreprocessorPipelineTest(unittest.TestCase):
    def setUp(self):
        self.save_directory = 'test_preprocessor_pipeline_save'
        self.save_file = self.save_directory + '/' + 'save_file.preprocessor'
        if os.path.exists(self.save_directory):
            shutil.rmtree(self.save_directory)

        os.makedirs(self.save_directory)

        self.preprocessor1 = DummyPreprocessor()
        self.preprocessor2 = LogarithmTransform()
        self.dummy_data = np.array([[1, 2, 3], [4, 5, 6]])

    def test_fit(self):
        self.preprocessor1.fit = unittest.mock.MagicMock()
        self.preprocessor2.fit = unittest.mock.MagicMock()

        pipeline = PreprocessorPipeline([self.preprocessor1, self.preprocessor2])
        pipeline.fit(self.dummy_data)

        self.preprocessor1.fit.assert_called_with(self.dummy_data)
        self.preprocessor2.fit.assert_called_with(self.preprocessor1.transform(self.dummy_data))

    def test_transform(self):
        self.preprocessor1.transform = unittest.mock.MagicMock()
        self.preprocessor2.transform = unittest.mock.MagicMock()

        pipeline = PreprocessorPipeline([self.preprocessor1, self.preprocessor2])
        pipeline.fit(self.dummy_data)

        transformed = pipeline.transform(self.dummy_data)

        self.preprocessor1.transform.assert_called_with(self.dummy_data)
        self.preprocessor2.transform.assert_called_with(self.preprocessor1.transform(self.dummy_data))

    def test_inverse_transform(self):
        self.preprocessor1.inverse_transform = unittest.mock.MagicMock()
        self.preprocessor2.inverse_transform = unittest.mock.MagicMock()

        pipeline = PreprocessorPipeline([self.preprocessor1, self.preprocessor2])
        pipeline.fit(self.dummy_data)

        transformed = pipeline.inverse_transform(self.dummy_data)

        self.preprocessor2.inverse_transform.assert_called_with(self.dummy_data)
        self.preprocessor1.inverse_transform.assert_called_with(self.preprocessor2.inverse_transform(self.dummy_data))

    def test_save_load(self):
        # save
        pipeline = PreprocessorPipeline([self.preprocessor1, self.preprocessor2])
        pipeline.save(self.save_file)
        # load
        loaded = PreprocessorPipeline.load(self.save_file)

        self.assertTrue(pipeline == loaded)


if __name__ == '__main__':
    unittest.main()
