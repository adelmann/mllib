import os
import unittest
import numpy as np
from mllib.model import LogarithmTransform

class LogarithmTransformTest(unittest.TestCase):

    def setUp(self):
        # path to test saving and loading
        self.savefile = 'logarithm_transform_test.pk'
        # make sure the savd file is deleted before each test
        if os.path.exists(self.savefile):
            os.remove(self.savefile)

        # generate the test data in a reproducible way
        np.random.seed(439840)
        self.test_data = np.random.uniform(0.0001, 10592, (42, 6))
        #self.test_data = np.array([[1., 1., 1., 1., 1., 1.],
        #                           [2., 4., 8., 16., 32., 64.]])
        # columns to transform
        self.to_transform = [1, 3, 5]
        # instantiate the transformation
        self.trafo = LogarithmTransform(self.to_transform)

    def test_transform(self):
        '''test for ```LogarithmTransform.transform()```'''
        transformed = self.trafo.transform(self.test_data)

        for col in range(self.test_data.shape[1]):
            if col in self.to_transform:
                correct = np.log(self.test_data[:, col])
            else:
                correct = self.test_data[:, col]

            self.assertTrue(np.isclose(transformed[:, col], correct).all())

    def test_consistency(self):
        '''test for ```LogarithmTransform.inverse_transform()```'''
        transformed = self.trafo.transform(self.test_data)
        transformed = self.trafo.inverse_transform(transformed)

        self.assertTrue(np.isclose(transformed, self.test_data).all())

    def test_saving(self):
        '''test for ```LogarithmTransform.save()``` and ```LogarithmTransform.load()```'''
        self.trafo.save(self.savefile)
        loaded = LogarithmTransform.load(self.savefile)

        self.assertTrue(self.trafo == loaded)

if __name__ == '__main__':
    unittest.main()
