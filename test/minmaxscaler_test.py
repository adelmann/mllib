import os
import unittest
import numpy as np
import pandas as pd
from mllib.model import AdaptiveMinMaxScaler, FixedMinMaxScaler

class AdaptiveMinMaxScalerTest(unittest.TestCase):
    def setUp(self):
        self._save_file = 'adapativeminmaxscaler_test.pk'

        if os.path.exists(self._save_file):
            os.remove(self._save_file)

        np.random.seed(329749530)
        self.uniform_data = np.random.uniform(-3.1, 5, size=(312, 91))
        normal_data = np.random.normal(loc=5., scale=33, size=75)
        normal_data2 = np.random.normal(loc=2, scale=100, size=75)
        data = {
            'normal col 1': normal_data,
            'normal_col 2': normal_data2
        }
        self.normal_dataframe = pd.DataFrame(data)

    def test_scaling_of_array(self):
        scaler = AdaptiveMinMaxScaler(-1, 1)
        scaler.fit(self.uniform_data)
        transformed = scaler.transform(self.uniform_data)

        # allow numerical tolerance
        self.assertTrue(((-1 <= transformed) | np.isclose(transformed, -1)).all())
        self.assertTrue(((transformed <= 1.) | np.isclose(transformed, 1)).all())
        self.assertTrue(self.uniform_data.shape == transformed.shape)

    def test_scaling_of_dataframe(self):
        scaler = AdaptiveMinMaxScaler(-1, 1)
        scaler.fit(self.normal_dataframe)
        transformed = scaler.transform(self.normal_dataframe)

        self.assertIsInstance(transformed, pd.DataFrame)

        self.assertTrue(((-1 <= transformed) | np.isclose(transformed, -1)).all().all())
        self.assertTrue(((transformed <= 1.) | np.isclose(transformed, 1)).all().all())
        self.assertTrue((self.normal_dataframe.columns == transformed.columns).all())
        self.assertTrue(self.normal_dataframe.shape == transformed.shape)

    def test_saving(self):
        scaler = AdaptiveMinMaxScaler(-1, 1)
        scaler.fit(self.normal_dataframe)
        transformed = scaler.transform(self.normal_dataframe)

        scaler.save(self._save_file)
        loaded_scaler = AdaptiveMinMaxScaler.load(self._save_file)
        transformed_by_loaded = loaded_scaler.transform(self.normal_dataframe)

        self.assertTrue(np.isclose(transformed_by_loaded, transformed).all())


class FixedMinMaxScalerTest(unittest.TestCase):

    def setUp(self):
        self._save_file = 'fixedminmaxscaler_test.pk'

        if os.path.exists(self._save_file):
            os.remove(self._save_file)

        np.random.seed(329749530)

        self.bounds = {
            'col0': [-2, 5],
            'col1': [123, 456]
        }

        self.uniform_data0 = np.random.uniform(self.bounds['col0'][0], self.bounds['col0'][1], size=(312))
        self.uniform_data1 = np.random.uniform(self.bounds['col1'][0], self.bounds['col1'][1], size=(312))

        self.data2D = np.stack([self.uniform_data0, self.uniform_data1], axis=1)
        self.columns = ['col0', 'col1']

        n_samples = 11
        n_timesteps = 42
        self.uniform_data0_3D = np.random.uniform(self.bounds['col0'][0], self.bounds['col0'][1], size=(n_samples, n_timesteps))
        self.uniform_data1_3D = np.random.uniform(self.bounds['col1'][0], self.bounds['col1'][1], size=(n_samples, n_timesteps))

        self.data3D = np.stack([self.uniform_data0_3D, self.uniform_data1_3D], axis=2)

    def test_scaling_of_arrays_2D(self):
        scaler = FixedMinMaxScaler(self.bounds, self.columns, a=-1., b=1.)
        scaler.fit(self.data2D)
        transformed = scaler.transform(self.data2D)

        # allow numerical tolerance
        self.assertTrue(((-1 <= transformed) | np.isclose(transformed, -1)).all().all())
        self.assertTrue(((transformed <= 1.) | np.isclose(transformed, 1)).all().all())
        self.assertTrue(transformed.shape[0] == self.uniform_data0.shape[0])
        self.assertTrue(transformed.shape[1] == 2)

    def test_inverse_scaling_of_arrays_2D(self):
        scaler = FixedMinMaxScaler(self.bounds, self.columns, a=-1., b=1.)
        scaler.fit(self.data2D)
        transformed = scaler.inverse_transform(scaler.transform(self.data2D))

        # allow numerical tolerance
        self.assertTrue(np.isclose(transformed, self.data2D).all())

    def test_scaling_of_arrays_3D(self):
        scaler = FixedMinMaxScaler(self.bounds, self.columns, a=-1., b=1.)
        scaler.fit(self.data3D)
        transformed = scaler.transform(self.data3D)

        # allow numerical tolerance
        self.assertTrue(((-1 <= transformed) | np.isclose(transformed, -1)).all().all())
        self.assertTrue(((transformed <= 1.) | np.isclose(transformed, 1)).all().all())
        self.assertTrue(transformed.shape[0] == self.uniform_data0_3D.shape[0])
        self.assertTrue(transformed.shape[1] == self.uniform_data0_3D.shape[1])
        self.assertTrue(transformed.shape[2] == 2)

    def test_inverse_scaling_of_arrays_3D(self):
        scaler = FixedMinMaxScaler(self.bounds, self.columns, a=-1., b=1.)
        scaler.fit(self.data3D)
        transformed = scaler.inverse_transform(scaler.transform(self.data3D))

        # allow numerical tolerance
        self.assertTrue(np.isclose(transformed, self.data3D).all())

    def test_saving(self):
        scaler = FixedMinMaxScaler(self.bounds, self.columns, a=-1., b=1.)
        scaler.fit(self.data3D)
        transformed = scaler.transform(self.data3D)

        scaler.save(self._save_file)
        loaded_scaler = AdaptiveMinMaxScaler.load(self._save_file)
        transformed_by_loaded = loaded_scaler.transform(self.data3D)

        self.assertTrue(np.isclose(transformed_by_loaded, transformed).all())


if __name__ == '__main__':
    unittest.main()
