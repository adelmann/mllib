import unittest
import numpy as np
import pandas as pd
from mllib.model import Standardize

class StandardizeTest(unittest.TestCase):
    def setUp(self):
        np.random.seed(329749530)
        normal_data = np.random.normal(loc=5., scale=33, size=75)
        normal_data2 = np.random.normal(loc=2, scale=100, size=75)
        normal_data3 = np.random.normal(loc=2, scale=10, size=75)
        
        data = {
            'normal:col1': normal_data,
            'normal:col2': normal_data2,
            'globalDate': normal_data,
            'attribute': normal_data3
        }
        
        data2 = {
            'normal:col1': normal_data,
            'normal:col2': normal_data2,
        }
        self.normal_dataframe = pd.DataFrame(data)
        self.normal_dataframe_noattributes = pd.DataFrame(data2)
        

    def test_scaling_of_dataframe(self):
        scaler = Standardize(remove_attributes=False)
        scaler.fit(self.normal_dataframe)
        transformed = scaler.transform(self.normal_dataframe)

        self.assertIsInstance(transformed, pd.DataFrame)
        self.assertTrue(( np.isclose(0, transformed.loc[:,'normal:col1':'normal:col2'].mean()).all().all()))
        self.assertTrue(( np.isclose(1, transformed.loc[:,'normal:col1':'normal:col2'].std(), atol=1e-2).all().all()))
        self.assertTrue((self.normal_dataframe.columns == transformed.columns).all())
        self.assertTrue(self.normal_dataframe.shape == transformed.shape)
        
    def test_remove_attributes(self):
        scaler = Standardize(remove_attributes=True)
        scaler.fit(self.normal_dataframe)
        transformed = scaler.transform(self.normal_dataframe)

        self.assertIsInstance(transformed, pd.DataFrame)
        self.assertTrue(( np.isclose(0, transformed.loc[:,'normal:col1':'normal:col2'].mean()).all().all()))
        self.assertTrue(( np.isclose(1, transformed.loc[:,'normal:col1':'normal:col2'].std(), atol=1e-2).all().all()))
        self.assertTrue((self.normal_dataframe_noattributes.columns == transformed.columns).all())
        self.assertTrue(self.normal_dataframe_noattributes.shape == transformed.shape)

if __name__ == '__main__':
    unittest.main()
