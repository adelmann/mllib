import os
import unittest
import numpy as np
import pandas as pd
from mllib.model import ShiftPositive

class ShiftPositiveTest(unittest.TestCase):
    def setUp(self):
        self._save_file = 'shiftpositive_test.pk'

        if os.path.exists(self._save_file):
            os.remove(self._save_file)

        np.random.seed(329749530)
        self.uniform_data = np.random.uniform(-3.1, 5, size=(312, 91))
        self.normal_data = np.random.normal(loc=5., scale=33, size=75)

    def test_positivity_of_array(self):
        for data in [self.uniform_data, self.normal_data]:
            scaler = ShiftPositive()
            scaler.fit(data)
            transformed = scaler.transform(data)

            self.assertTrue((transformed > 0.).all())
            self.assertTrue(data.shape == transformed.shape)
        
    def test_consistency(self):
        scaler = ShiftPositive()
        scaler.fit(self.uniform_data)
        transformed = scaler.transform(self.uniform_data)
        transformed_back = scaler.inverse_transform(transformed)
        
        self.assertTrue(np.isclose(transformed_back, self.uniform_data).all())

    def test_saving(self):
        scaler = ShiftPositive()
        scaler.fit(self.uniform_data)
        transformed = scaler.transform(self.uniform_data)

        scaler.save(self._save_file)
        loaded_scaler = ShiftPositive.load(self._save_file)
        transformed_by_loaded = loaded_scaler.transform(self.uniform_data)

        self.assertTrue(np.isclose(transformed_by_loaded, transformed).all())

if __name__ == '__main__':
    unittest.main()
