import unittest
import numpy as np
import pandas as pd
from mllib.model import TrainTestValSplit

class TrainTestValSplitTest(unittest.TestCase):
    def setUp(self):
        np.random.seed(329749530)
        normal_data = np.random.normal(loc=5., scale=33, size=24)
        normal_data2 = np.random.normal(loc=2, scale=100, size=24)
        targets = [-1,-1,-1,-1,1,1,1,1,1,1,1,1,-1,-1,-1,-1,1,1,1,1,1,1,1,1]
        windowindices = [0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5]
        self._onetarget = [-1,1,1,-1,1,1]
        
        data1 = {
            'normal:col1': normal_data,
            'normal:col2': normal_data2,
            'globalDate' : normal_data,
            'windowIndex': windowindices,
            'TargetLabel': targets
        }
        
        data2 = {
            'normal:col1': normal_data,
            'normal:col2': normal_data2
        }
        
        self.all_dataframe = pd.DataFrame(data1)
        self.noattributes_dataframe = pd.DataFrame(data2)
        
    def test_column_handling_threesplits_noattributes(self):
        splitter = TrainTestValSplit(num_splits=3,remove_attributes =True, retain_timeorder_over_sets = False)
        splitter.fit(self.all_dataframe)
        train,y_train, test, y_test, val, y_val = splitter.transform(self.all_dataframe)
        
        self.assertIsInstance(train, pd.DataFrame)
        self.assertIsInstance(test, pd.DataFrame)
        self.assertIsInstance(val, pd.DataFrame)
        
        #check attribute removal
        self.assertTrue((self.noattributes_dataframe.columns == train.columns).all())
        self.assertTrue((self.noattributes_dataframe.columns == test.columns).all())
        self.assertTrue((self.noattributes_dataframe.columns == val.columns).all())
        

    def test_column_handling_twosplits_noattributes(self):
        splitter = TrainTestValSplit(num_splits=2,remove_attributes =True, retain_timeorder_over_sets = False)
        splitter.fit(self.all_dataframe)
        train,y_train, test, y_test = splitter.transform(self.all_dataframe)
        
        self.assertIsInstance(train, pd.DataFrame)
        self.assertIsInstance(test, pd.DataFrame)
        
        #check attribute removal
        self.assertTrue((self.noattributes_dataframe.columns == train.columns).all())
        self.assertTrue((self.noattributes_dataframe.columns == test.columns).all())   
    
    def test_column_handling_threesplits(self):
        splitter = TrainTestValSplit(num_splits=3,remove_attributes =False, retain_timeorder_over_sets = False)
        splitter.fit(self.all_dataframe)
        train,y_train, test, y_test, val, y_val = splitter.transform(self.all_dataframe)
        
        self.assertIsInstance(train, pd.DataFrame)
        self.assertIsInstance(test, pd.DataFrame)
        self.assertIsInstance(val, pd.DataFrame)
        
        #check attribute removal
        self.assertTrue((self.all_dataframe.columns == train.columns).all())
        self.assertTrue((self.all_dataframe.columns == test.columns).all())
        self.assertTrue((self.all_dataframe.columns == val.columns).all())
        
        #check that target order is correct
        self.assertTrue((train['TargetLabel'].tolist() == y_train).all())
        self.assertTrue((test['TargetLabel'].tolist() == y_test).all())
        self.assertTrue((val['TargetLabel'].tolist() == y_val).all())
        
    def test_column_handling_twosplits(self):
        splitter = TrainTestValSplit(num_splits=2,remove_attributes =False, retain_timeorder_over_sets = False)
        splitter.fit(self.all_dataframe)
        train,y_train, test, y_test = splitter.transform(self.all_dataframe)
        
        self.assertIsInstance(train, pd.DataFrame)
        self.assertIsInstance(test, pd.DataFrame)
        
        #check attribute removal
        self.assertTrue((self.all_dataframe.columns == train.columns).all())
        self.assertTrue((self.all_dataframe.columns == test.columns).all())
        
        #check that target order is correct
        self.assertTrue((train['TargetLabel'].tolist() == y_train).all())
        self.assertTrue((test['TargetLabel'].tolist() == y_test).all())
        
    def test_timeorder_preservation(self):
        #for the 2 split case
        splitter = TrainTestValSplit(num_splits=2,remove_attributes =False, retain_timeorder_over_sets = True)
        splitter.fit(self.all_dataframe)
        train,y_train, test, y_test = splitter.transform(self.all_dataframe)
        df_all = train.append(test)
        
        self.assertTrue(np.isclose(self.all_dataframe['globalDate'].values, df_all['globalDate'].values).all())
        
        #for the 3 split case
        splitter = TrainTestValSplit(num_splits=3,remove_attributes =False, retain_timeorder_over_sets = True)
        splitter.fit(self.all_dataframe)
        train,y_train, test, y_test, val, y_val = splitter.transform(self.all_dataframe)
        
        df_all = train.append(test)
        df_all = df_all.append(val)

        self.assertTrue(np.isclose(self.all_dataframe['globalDate'].values,df_all['globalDate'].values).all())
        
    def test_onetarget(self):
        #for the 2 split case
        splitter = TrainTestValSplit(num_splits=2,remove_attributes =False, one_target_per_window=True)
        splitter.fit(self.all_dataframe)
        train,y_train, test, y_test = splitter.transform(self.all_dataframe)
        
        for i in y_test:
            y_train.append(i)
        
        self.assertTrue(np.all(y_train == self._onetarget))





if __name__ == '__main__':
    unittest.main()
