import os
import unittest
import numpy as np
import pandas as pd
from mllib.model import StandardScaler

class StandardScalerTest(unittest.TestCase):
    def setUp(self):
        self._save_file = 'standardscaler_test.pk'

        if os.path.exists(self._save_file):
            os.remove(self._save_file)

        np.random.seed(329749530)
        self.uniform_data = np.random.uniform(-3.1, 5, size=(312, 91))
        normal_data = np.random.normal(loc=5., scale=33, size=75)
        normal_data2 = np.random.normal(loc=2, scale=100, size=75)
        data = {
            'normal col 1': normal_data,
            'normal_col 2': normal_data2
        }
        self.normal_dataframe = pd.DataFrame(data)

    def test_scaling_of_array(self):
        scaler = StandardScaler()
        scaler.fit(self.uniform_data)
        transformed = scaler.transform(self.uniform_data)

        # allow numerical tolerance
        self.assertTrue(np.all(np.isclose(np.std(transformed, axis=0), 1.)))
        self.assertTrue(np.all(np.isclose(np.mean(transformed, axis=0), 0.)))

    def test_scaling_of_dataframe(self):
        scaler = StandardScaler()
        scaler.fit(self.normal_dataframe)
        transformed = scaler.transform(self.normal_dataframe)

        self.assertIsInstance(transformed, pd.DataFrame)

        self.assertTrue(np.all(np.isclose(np.std(transformed, axis=0), 1.)))
        self.assertTrue(np.all(np.isclose(np.mean(transformed, axis=0), 0.)))
        self.assertTrue((self.normal_dataframe.columns == transformed.columns).all())
        self.assertTrue(self.normal_dataframe.shape == transformed.shape)

    def test_saving(self):
        scaler = StandardScaler()
        scaler.fit(self.normal_dataframe)
        transformed = scaler.transform(self.normal_dataframe)

        scaler.save(self._save_file)
        loaded_scaler = StandardScaler.load(self._save_file)
        transformed_by_loaded = loaded_scaler.transform(self.normal_dataframe)

        self.assertTrue(np.isclose(transformed_by_loaded, transformed).all())

if __name__ == '__main__':
    unittest.main()
